import './assets/styles/App.css';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Navbar from "./components/navbar";
import Footer from "./components/Footer";
import Home from "./views/Home";
import Model from "./views/Model";
import Agents from "./views/Agents";
import Simulation from "./views/Simulation";



function App() {
  return (
    <Router>
        <Navbar />
      <Routes>
        <Route index path='/' element={<Home/>} />
        <Route path='/model' element={<Model/>} />
        <Route path='/agents' element={<Agents/>} />
        <Route path='/simulation' element={<Simulation/>} />
      </Routes>
    <Footer />
    </Router>

  );
}

export default App;
