import React from "react";
import {
    Box,
    Container,
    Row,
    Column,
    FooterLink,
    Heading,
} from "./FooterStyle";
require("../assets/styles/footer.css");

const Footer = () => {
    return (
        <Box className={"footer"}>
            <Container>
                <Row>
                    <Column>
                        <Heading>About WoW</Heading>
                        <FooterLink href="https://gitlab.utc.fr/kryusant/wow" target="_blank">GitLab repo</FooterLink>
                        <FooterLink href="https://gitlab.utc.fr/kryusant/wow/-/blob/413a34eb907e794cce15fd9a035b176c7f08fc0a/LICENSE" target="_blank">Open-Source licence</FooterLink>
                    </Column>
                    <Column>
                        <Heading>Contact us</Heading>
                        <FooterLink href="#">Send us an email</FooterLink>
                        <FooterLink href="#">Our Gitlab profiles</FooterLink>
                    </Column>
                </Row>
            </Container>
        </Box>
    );
};
export default Footer;