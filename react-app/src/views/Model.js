import icon1 from '../assets/img/icon_wow1.png'
import interactWow from '../assets/img/agents_interactions_wow.png'
import interactDosi from '../assets/img/agents_interactions_dosi.png'
import miucc from '../assets/doc/Designing_a_platform_for_the_impact_of_AI_on_work_WoW.ai_jul_2022.pdf'

import React from 'react';
import { Link } from 'react-router-dom'


const Model = () =>{


  return(
    <div className="App">

      <div className="page">
        <div className="separation">
          <text><text id="logo">WoW<text id="cyan">.ai</text></text>'s current model description</text>
        </div>
        <img src={icon1} className="bottom-right-img icon1" alt="WorkOnWork icon with smiling emojis" />

          <div className="model-div">

            <h3>Introduction</h3>
            The current model is mostly based on <i>Dosi, et. al., 2022</i> model (after referenced as the "original model") with our own modifications. Indeed, our prototype is only a simplified version of it for the sake of having something at first, because, this is the characteristic of agent-based model, the complexity can be infinite provided that there is no "overfitting" bias (validation is necessary for that).
            <br/><br/>
            This model is almost purely quantitative and answer at first to neoclassical economics questions while also beeing modular in order to integrate more specific questions. For example, the current model could easily integrate a basic income to see its effect.
            <br/><br/>
            <h3>Model settings</h3>
            There are for the moment 4 types of economic agents in our model and 1 coordinator agent, in addition to the technology structure.
            <br/><br/>

            Our first simuations ran with 25,000 workers, 25 consumption-good firms (that produces goods), and 10 capital-goods firms (that produces technologies), and one government, while the original model runs with 250,000 workers, 50 consumption-goods firms and 20 capital-goods firms, ten banks, one central banks, one government and initially 6 differentiated consumption-goods industries.
            You can see a complete description of the model by agents in the  <Link to="/agents">agents page</Link>.
            The simplification of the model is necessary at first to be able obtain correct economic pattern with calibration.
            <br/><br/>

            The projects works by sequentially simulating a set of <strong>steps</strong> that is crucial in economics to understand the evolution of income, repartition, and also other quantitative variable as the skills of the workers that stands here for their potential productivity. They are as follow in the current model:

            <ol>
              <li>Scientific research happens</li>
              <li>Workers (employed and unemployed) update their skills</li>
              <li>Sets of technologies ordered in the previous period (if any) are delivered</li>
              <li>Capital-good firm perform R&D (incremental and radical innovation, and imitation) and send signal the new machines to the consumption-good firms</li>
              <li>Consumption-good firms and capital-goods firms determine desired production (according to consumption or orders), investments and workforce.</li>
              <li>Consumption-goods firms invest: machines from capital-good sector are ordered for next period by consumption-goods firms.</li>
              <li>Job-seeking workers send a set of job applications</li>
              <li>Firms hire candidates and skills adapts</li>
              <li>Firms pay wages/bonuses and government pays unemployment benefits (and firms compute average wage)</li>
              <li>Consumption of workers occurs</li>
              <li>Firms compute their profits</li>
              <li>Aggregate variable are computed (moslty by the coordinator agent)</li>
            </ol>

            The simulation runs on t periods corresponding to one sequence of all these steps. We ran our first tries with 100 periods successfully.


            <h3>Critiques and evolution of the model</h3>

            The model is very centered around the economic data while the WoW project aimed at giving also qualitative meanings: will people be satisfied (not in the economic sense but in term of psychology) with technologies?
            Thus, a crucial evolution for the project is to question if the compatibility of the economic phenomenons in quantitative models with the integration of sociological, psychological, or legal aspects in the agents characteristics.
            <br/><br/>
            The model supposes a too big rationnality and sometimes a near perfection of the informaition: some economic values from the point of view of the firm are computed from
            A more bounded rationnality is needed to custom the agents choices according to their near environment and from the information they have. It is sometimes alread done by stochastic process as the choice of companies to apply to by the workers are choosed randomly (with the <a href="https://en.wikipedia.org/wiki/Rejection_sampling" target="_blank">rejection sampling</a> method).
            Several criteria could have been choosen such as the geographical data to choose the company, the gender, etc., but the probabilistic criteria aims at giving a simplification of the choices. See <Link to="/agents">agents page</Link> to have a wider vision of all the ciritcs and model hyptohesis.
            <br/><br/>
          </div>
        <div className="separation">
          <text><text id="logo">WoW<text id="cyan">.ai</text></text>'s interactions between agents</text>
        </div>
        <div className="interactions-div">
        <h3>Current model interactions</h3>
        This diagram represents the current model interactions, that suggest the simplified representation of the econommy, but still meaningful to represent economic stylized facts.
        The interactions are represented by a direct arrow with an action that is from one agent to one another, or by a mutual interaction represented by an event (<i>e.g.</i> consumption).

        <br/><br/>
          <img src={interactWow} className="png-interactions" alt="WorkOnWork current interactions diagram" />

        <h3>Interactions in a complete evolutionnary model</h3>

        This diagram shows more advanced interactions, that were draw from <i>Dosi et. al., 2022</i>, and stands for a complete evolutionnary model in economics of innovation throughout industrial dynamics.

          <br/><br/>


          <img src={interactDosi} className="png-interactions" alt="WorkOnWork goal interactions diagram" />


        </div>

        <div className="separation">
          <text>Bibliography: <text id="logo">WoW<text id="cyan">.ai</text></text>'s sources</text>
        </div>
        <div className="biblio-div">

        Here is presented a short bibliography on what was necessary to build the model. Every document was not fully used and can be taken as a complement to better understand the model at the economic model but also at the multi-agent system level.

        <br/><br/>

        The <strong>first report made on the project</strong> set the design thinking methodology and the project process. It can be found at <a href={miucc} target="_blank">this link</a>.
        A deeper bibliography, especially on the project spirit, can then be found on it.

        <h3>Agent-based Computational Economics generalities</h3>

          <div class="csl-bib-body">
            <div data-csl-entry-id="aghion_direct_nodate" class="csl-entry">Aghion, P., Antonin, C., Bunel, S., &#38; Jaravel, X. (n.d.). <i>The Direct and Indirect Effects of Automation on Employment: A Survey of the Recent Literature</i>. <a href="https://scholar.harvard.edu/files/aghion/files/direct_and_indirect_effects_of_automation.pdf" target="_blank">https://scholar.harvard.edu/files/aghion/files/direct_and_indirect_effects_of_automation.pdf</a></div>
            <div data-csl-entry-id="delli_gatti_agent-based_2018" class="csl-entry">Delli Gatti, D., &#38; et.,  al. (2018). <i>Agent-Based Models in Economics: A Toolkit</i>. Cambridge University Press. <a href="https://www.jasss.org/21/4/reviews/3.html" target="_blank">https://www.jasss.org/21/4/reviews/3.html</a></div>
            <div data-csl-entry-id="dosi_technological_2022" class="csl-entry">Dosi, G., Pereira, M. C., Roventini, A., &#38; Virgillito, M. E. (2022). Technological paradigms, labour creation and destruction in a multi-sector agent-based model. <i>Research Policy</i>, <i>51</i>(10), 104565. <a href="https://doi.org/10.1016/j.respol.2022.104565" target="_blank">https://doi.org/10.1016/j.respol.2022.104565</a></div>
            <div data-csl-entry-id="goudet_modelisation_2015" class="csl-entry">Goudet, O. (2015). <i>La modélisation multi-agent du marché du travail français</i> [Phdthesis, Université Pierre et Marie Curie]. <a href="https://tel.archives-ouvertes.fr/tel-01343302" target="_blank">https://tel.archives-ouvertes.fr/tel-01343302</a></div>
            <div data-csl-entry-id="kant_worksim_2020" class="csl-entry">Kant, J.-D., Ballot, G., &#38; Goudet, O. (2020). WorkSim: An Agent-Based Model of Labor Markets. <i>Journal of Artificial Societies and Social Simulation</i>.</div>
            <div data-csl-entry-id="noauthor_long_1981" class="csl-entry">Long waves, inventions, and innovations. (1981). <i>Futures</i>, <i>13</i>(4), 308–322. <a href="https://doi.org/10.1016/0016-3287(81)90146-4" target="_blank">https://doi.org/10.1016/0016-3287(81)90146-4</a></div>
            <div data-csl-entry-id="napoletano_les_2017" class="csl-entry">Napoletano, M. (2017). Les modèles multi-agents et leurs conséquences pour l’analyse macroéconomique. <i>Revue de l’OFCE</i>, <i>153</i>(4), 289–316. <a href="https://doi.org/10.3917/reof.153.0289" target="_blank">https://doi.org/10.3917/reof.153.0289</a></div>
            <div data-csl-entry-id="noauthor_pdf_nodate" class="csl-entry">(PDF) The Skill Content Of Recent Technological Change: An Empirical Exploration. (n.d.). <i>ResearchGate</i>. <a href="https://doi.org/10.1162/003355303322552801" target="_blank">https://doi.org/10.1162/003355303322552801</a></div>
            <div data-csl-entry-id="noauthor_schumpeter_2010" class="csl-entry">Schumpeter meeting Keynes: A policy-friendly model of endogenous growth and business cycles. (2010). <i>Journal of Economic Dynamics and Control</i>, <i>34</i>(9), 1748–1767. <a href="https://doi.org/10.1016/j.jedc.2010.06.018" target="_blank">https://doi.org/10.1016/j.jedc.2010.06.018</a></div>
            <div data-csl-entry-id="seppecher_modeles_2016" class="csl-entry">Seppecher, P. (2016). <i>Modèles multi-agents et stock-flux cohérents : une convergence logique et nécessaire</i>. <a href="https://hal.archives-ouvertes.fr/hal-01309361" target="_blank">https://hal.archives-ouvertes.fr/hal-01309361</a></div>
            <div data-csl-entry-id="tesfatsion_agent-based_nodate" class="csl-entry">Tesfatsion, L. (n.d.). <i>Agent-Based Modeling: The Right Mathematics for Social Science?</i> 47.</div>
            <div data-csl-entry-id="tesfatsion_agent-based_2006" class="csl-entry">Tesfatsion, L. (2006). Agent-Based Computational Economics: A Constructive Approach to Economic Theory. <i>Economics Department, Iowa State University, Ames, IA 50011-1070</i>. <a href="http://www.econ.iastate.edu/tesfatsi/hbintlt.pdf" target="_blank">http://www.econ.iastate.edu/tesfatsi/hbintlt.pdf</a></div>
            <div data-csl-entry-id="noauthor_labour-augmented_2020" class="csl-entry">The labour-augmented K + S model: A laboratory for the analysis of institutional and policy regimes. (2020). <i>EconomiA</i>, <i>21</i>(2), 160–184. <a href="https://doi.org/10.1016/j.econ.2019.03.002" target="_blank">https://doi.org/10.1016/j.econ.2019.03.002</a></div>
            <div data-csl-entry-id="noauthor_oxford_2018" class="csl-entry"><i>The Oxford Handbook of Computational Economics and Finance</i>. (2018). <a href="https://doi.org/10.1093/oxfordhb/9780199844371.001.0001" target="_blank">https://doi.org/10.1093/oxfordhb/9780199844371.001.0001</a></div>
            <div data-csl-entry-id="noauthor_training_nodate" class="csl-entry">Training policies and economic growth in an evolutionary world | Request PDF. (n.d.). <i>ResearchGate</i>. <a href="https://doi.org/10.1016/S0954-349X(01)00023-6" target="_blank">https://doi.org/10.1016/S0954-349X(01)00023-6</a></div>
          </div>

          <h3>Digital automation models (not only ABM)</h3>

          <div class="csl-bib-body">
            <div data-csl-entry-id="acemoglu_modeling_2018" class="csl-entry">Acemoglu, D., &#38; Restrepo, P. (2018). Modeling Automation. <i>AEA Papers and Proceedings</i>, <i>108</i>, 48–53. <a href="https://doi.org/10.1257/pandp.20181020" target="_blank">https://doi.org/10.1257/pandp.20181020</a></div>
            <div data-csl-entry-id="anzolin_automation_2021" class="csl-entry">Anzolin, G. (2021). <i>Automation and its employment effects: a literature review of automotive and garment sectors</i> [Publication]. <a href="http://www.ilo.org/employment/Whatwedo/Projects/building-partnerships-on-the-future-of-work/news/WCMS_823347/lang--en/index.htm" target="_blank">http://www.ilo.org/employment/Whatwedo/Projects/building-partnerships-on-the-future-of-work/news/WCMS_823347/lang--en/index.htm</a></div>
            <div data-csl-entry-id="bertani_productivity_2020" class="csl-entry">Bertani, F., Raberto, M., &#38; Teglio, A. (2020). The productivity and unemployment effects of the digital transformation: an empirical and modelling assessment. <i>Review of Evolutionary Political Economy</i>, <i>1</i>(3), 329–355. <a href="https://doi.org/10.1007/s43253-020-00022-3" target="_blank">https://doi.org/10.1007/s43253-020-00022-3</a></div>
            <div data-csl-entry-id="chiaromonte_heterogeneity_1993" class="csl-entry">Chiaromonte, F., &#38; Dosi, G. (1993). Heterogeneity, competition, and macroeconomic dynamics. <i>Structural Change and Economic Dynamics</i>, <i>4</i>(1), 39–63. <a href="https://doi.org/10.1016/0954-349X(93)90004-4" target="_blank">https://doi.org/10.1016/0954-349X(93)90004-4</a></div>
            <div data-csl-entry-id="dengler_impacts_2018" class="csl-entry">Dengler, K., &#38; Matthes, B. (2018). The impacts of digital transformation on the labour market: Substitution potentials of occupations in Germany. <i>Technological Forecasting and Social Change</i>, <i>137</i>, 304–316. <a href="https://doi.org/10.1016/j.techfore.2018.09.024" target="_blank">https://doi.org/10.1016/j.techfore.2018.09.024</a></div>
            <div data-csl-entry-id="deschacht_digital_2021" class="csl-entry">Deschacht, N. (2021). The digital revolution and the labour economics of automation: A review. <i>ROBONOMICS: The Journal of the Automated Economy</i>, <i>1</i>, 8–8. <a href="https://journal.robonomics.science/index.php/rj/article/view/8" target="_blank">https://journal.robonomics.science/index.php/rj/article/view/8</a></div>
            <div data-csl-entry-id="dosi_fiscal_2015" class="csl-entry">Dosi, G., Fagiolo, G., Napoletano, M., Roventini, A., &#38; Treibich, T. (2015). Fiscal and monetary policies in complex evolving economies. <i>Journal of Economic Dynamics and Control</i>, <i>52</i>, 166–189. <a href="https://doi.org/10.1016/j.jedc.2014.11.014" target="_blank">https://doi.org/10.1016/j.jedc.2014.11.014</a></div>
            <div data-csl-entry-id="dosi_short-and_2014" class="csl-entry">Dosi, G., Napoletano, M., Roventini, A., &#38; Treibich, T. (2014). <i>The Short-and Long-Run Damages of Fiscal Austerity: Keynes beyond Schumpeter</i>.</div>
            <div data-csl-entry-id="noauthor_economics_2009" class="csl-entry"><i>Economics 302 Intermediate Macroeconomic Theory and Policy (Lecture 21-22)</i>. (2009). University of Wisconsin-Madison.</div>
            <div data-csl-entry-id="noauthor_eurace_2008" class="csl-entry">EURACE: A massively parallel agent-based model of the European economy. (2008). <i>Applied Mathematics and Computation</i>, <i>204</i>(2), 541–552. <a href="https://doi.org/10.1016/j.amc.2008.05.116" target="_blank">https://doi.org/10.1016/j.amc.2008.05.116</a></div>
            <div data-csl-entry-id="noauthor_figure_nodate" class="csl-entry">Figure 1: The structure of the Keynes+Schumpeter model. (n.d.). In <i>ResearchGate</i>. Retrieved November 11, 2022, from <a href="https://www.researchgate.net/figure/The-structure-of-the-Keynes-Schumpeter-model_fig1_270586057" target="_blank">https://www.researchgate.net/figure/The-structure-of-the-Keynes-Schumpeter-model_fig1_270586057</a></div>
            <div data-csl-entry-id="giancarlo_keynes_2019" class="csl-entry">Giancarlo, B., &#38; Kalajzić, A. (2019). A Keynes + Schumpeter model to explain development, speculation and crises POST-KEYNESIAN ECONOMICS SOCIETY A Keynes + Schumpeter model to explain development, speculation and crises A Keynes+Schumpeter model to explain development, speculation and crises. <i>Post-Keynesian Economics Society</i>.</div>
            <div data-csl-entry-id="korinek_artificial_2021" class="csl-entry">Korinek, A., &#38; Stiglitz, J. E. (2021). <i>Artificial Intelligence, Globalization, and Strategies for Economic Development</i> [SSRN Scholarly Paper]. <a href="https://papers.ssrn.com/abstract=3812820" target="_blank">https://papers.ssrn.com/abstract=3812820</a></div>
            <div data-csl-entry-id="romero_mr_2014" class="csl-entry">Romero, J. P. (2014). Mr. Keynes and the neo-Schumpeterians: Contributions to the analysis of the determinants of innovation from a post-Keynesian perspective. <i>EconomiA</i>, <i>15</i>(2), 189–205. <a href="https://doi.org/10.1016/j.econ.2014.06.001" target="_blank">https://doi.org/10.1016/j.econ.2014.06.001</a></div>
          </div>

          <h3>Digital automation policies based on these model, for the stylized facts</h3>

          <div class="csl-bib-body">
            <div data-csl-entry-id="korinek_artificial_2021" class="csl-entry">Korinek, A., &#38; Stiglitz, J. E. (2021). <i>Artificial Intelligence, Globalization, and Strategies for Economic Development</i> [SSRN Scholarly Paper]. <a href="https://papers.ssrn.com/abstract=3812820" target="_blank">https://papers.ssrn.com/abstract=3812820</a></div>
          </div>

          <h3>Data used to improve or calibrate the model</h3>

          <div class="csl-bib-body">
            <div data-csl-entry-id="noauthor_cinquante_nodate" class="csl-entry"><i>Cinquante ans de consommation alimentaire : une croissance modérée, mais de profonds changements - Insee Première - 1568</i>. (n.d.). Retrieved December 11, 2022, from <a href="https://www.insee.fr/fr/statistiques/1379769" target="_blank">https://www.insee.fr/fr/statistiques/1379769</a></div>
            <div data-csl-entry-id="noauthor_epargne_nodate" class="csl-entry"><i>Epargne : des inégalités énormes selon le niveau de vie – Centre d’observation de la société</i>. (n.d.). Retrieved December 17, 2022, from <a href="https://www.observationsociete.fr/revenus/epargne-revenu/" target="_blank">https://www.observationsociete.fr/revenus/epargne-revenu/</a></div>
            <div data-csl-entry-id="noauthor_tasks_2020" class="csl-entry">Tasks within occupations. (2020). In <i>CEDEFOP</i>. <a href="https://www.cedefop.europa.eu/en/tools/skills-intelligence/tasks-within-occupations" target="_blank">https://www.cedefop.europa.eu/en/tools/skills-intelligence/tasks-within-occupations</a></div>
            <div data-csl-entry-id="zwijnenburg_distribution_2021" class="csl-entry">Zwijnenburg, J., Bournot, S., Grahn, D., &#38; Guidetti, E. (2021). <i>Distribution of household income, consumption and saving in line with national accounts: Methodology and results from the 2020 collection round</i> [Techreport]. OECD. <a href="https://doi.org/10.1787/615c9eec-en" target="_blank">https://doi.org/10.1787/615c9eec-en</a></div>
          </div>
        </div>


    </div>
  </div>
  );
}
export default Model;
