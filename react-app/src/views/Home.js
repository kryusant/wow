import scene from '../assets/img/scene.png'
import action from '../assets/img/action.png'

import miucc from '../assets/doc/Designing_a_platform_for_the_impact_of_AI_on_work_WoW.ai_jul_2022.pdf'



import React from 'react';
const Home = () =>{
  return(
    <div className="App">
      <header className="App-header">
        <img src={scene} className="App-logo" alt="logo" />
        <p id="front-title">
          Work on <span style={{whiteSpace: "pre", color: "cyan"}}>Work:<u>                   </u></span><span className="blink">></span><text>&nbsp; &nbsp; A simulation of the impact of technologies on human work</text>
        </p>
      </header>
      <div className="page">
        <div className="questions">
          <div className="question-left">
            🤓 How can we bring political class a clear vision about the automation issue?
          </div>
          <div className="question-right">
          Does technology brings more labour or does it steal it from humans? 😶‍🌫️
          </div>
          <div className="question-left">
            🤔 Is there any way to simulate the effect of technology with other knowledge than economic ?
          </div>
          <div className="question-right">
          What is the impacts of models on society? 🧐
          </div>
          <div className="separation">
            About
          </div>
          <div className="about-div">
            <p>
              All those questions are here because the Work On Work projects aims to answer to all of these. WoW is a platform integrating a "digital twin of the economy" aiming at having more opinion on the employment problem, especially on automation expectations to structurally make the employment decreases.
            </p>
            <p>
              The platform project seeks to solve a strong need of companies to identify useful digitization from the
              superficial one, in their investments but also in their civil objectives. It also has as its raison d'être to face
              the huge problem of automation in the labor economy, which currently is mainly a market issue,
              a market obstructed by the modification but especially the reduction of tasks by for example artificial intelligence technologies.
            </p>
            <p>
              As mentioned in the <a href={miucc} target="_blank">first report</a>, there is indeed a risk of job losses of the order of 50% in regions such as
              regions such as Europe or North America, and it is this problem that the platform wishes to address. Effectively, across our information acquisition,
              we found that more complex economic pattern could show that the introduction of such technology increase employment but also harm specific class of workers.
            </p>
            <p>
              This platform is intended to act as a medium for crystallizing the discussion around automation and its impact on employment,
              as any technique can do. the Living Lab approach, among others, has an impact on public policies of public administrations on employment and in any case allows to increase their involvment.
              The approach is therefore highly critical - especially of the Californian or "Silicon Valley" model of entrepreneurial innovation (a network of companies linked to academic technological innovation, whose main aim is to
              is entrepreneurial success) and opts for an approach centered around the human linking the actors of the
              public.
            </p>
            <img src={action} id="action-phase" alt="Action phase description image"/>
            <p>
              The project therefore aims to develop the company by finding alternative methods of financing specific to the
              Living Lab methodology, capable of taking into account the needs of citizens other than by the voice (and not the way)
              monetary.
              Clearly, it is a question of integrating a strong democratic component to the elaboration and the use of the tool as well as the
              project behind it, which is to formulate policies adapted to the redesign of the labor society, for example through
              modes of economic redistribution that are not exclusively wage-earning, i.e. finding solutions at any cost.
            </p>
            <p>
              For the moment, as it is indicated on the bellow timeline, the project is in prototype step and therefore needs your help to pursue the goals!
            </p>
          </div>
          <div className="separation">
            Project goals
          </div>
          <div className="about-div">
            <p>
              The final objectives of the entire project are to work on :
            &nbsp;
              <ul>
                <li>A "digital twin" platform integrating:
                <ul>
                  <li>A multi-agent system (MAS) for simulating work trips due to so-called artificial intelligence technologies;</li>
                  <li>An accurate worker contact trigger when the lights turn red for the job or sector in question;</li>
                  <li>Data from recent economic sources but also from workers / employers;</li>
                </ul>
                </li>
                <li>An econometric tool for the platform variables;</li>
                <li>A simulation tool (also MAS) of the impact of digital technology on a job ;</li>
                <li>An institutional takeover project for the platform:
                <ul>
                  <li>Creating a collective or networking the necessary actors, leading to the creation of a living lab (or other type of structure);</li>
                  <li>use mainly by public administrations, companies, citizens, and researchers</li>
                </ul>
                </li>
              </ul>
            </p>
          </div>

          <div className="separation">
            Project steps
          </div>

            <div class="timeline">
              <div class="container left">
                <div class="date">Jun 2022</div>
                <i class="icon fa fa-home"></i>
                <div class="content">
                  <h2>Design thinking</h2>
                  <p>
                    Project settings and foundations, design thinking of the innovation process and environment (<a href={miucc} target="_blank">first report, UAB, Barcelona</a>).
                  </p>
                </div>
              </div>
              <div class="container right">
                <div class="date">Jan 2023</div>
                <i class="icon fa fa-gift"></i>
                <div class="content">
                  <h2>Prototype of a multi-agents system</h2>
                  <p>
                    Construction of this present website and of a first proof of concept (UTC, Compiègne) of an pure economic evolutionnary agent-based model from the <i>Dosi et. al., 2022</i> paper.
                    <ul>
                      <li>Researchers contacts</li>
                      <li>Economic bibliography</li>
                      <li>Model elaboration</li>
                      <li>Model construction in Go</li>
                      <li> Website construction</li>
                    </ul>
                  </p>
                </div>
              </div>
              <div class="container left not-done">
                <div class="date">Feb 2023</div>
                <i class="icon almost-done fa fa-user"></i>
                <div class="content">
                  <h2>Calibration and validation</h2>
                  <p>
                    End of the calibration and validation phase of the first prototype aiming at validating the model from the economic point of view.
                    <ul>
                      <li>Calibration (with/without data)</li>
                      <li>Monte Carlo validation</li>
                      <li>Empiracal cases validation</li>
                    </ul>
                  </p>
                </div>
              </div>
              <div class="container right not-done">
                <div class="date">Jul 2023</div>
                <i class="icon fa fa-running"></i>
                <div class="content">
                  <h2>Living lab collective formation</h2>
                  <p>
                    Community creation in order to extend the economic model to a more qualitative, interdisciplinary research and innovation process. Living lab methodology implementation.
                  </p>
                </div>
              </div>
              <div class="container left not-done">
                <div class="date">Feb 2024</div>
                <i class="icon fa fa-cog"></i>
                <div class="content">
                  <h2>Complexity improvement and politics propositions</h2>
                  <p>
                    Interdisciplinary integrations and complexity in the model (without reaching bias) and repeat the cycle of complexity improvements. Integrate the work in a more complete platform and present the work for economic advisory administration.
                  </p>
                </div>
              </div>
            </div>
        </div>
    </div>
    </div>
  );
}
export default Home;
