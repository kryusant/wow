import icon1 from '../assets/img/icon_wow1.png'
import worker from '../assets/img/worker.png'
import capFirm from '../assets/img/cap_firm.png'
import consFirm from '../assets/img/cons_firm.png'
import world from '../assets/img/world.png'
import gov from '../assets/img/gov.png'
import tech from '../assets/img/tech.png'


import ReactMarkdown from 'react-markdown'
import { MathJax, MathJaxContext } from "better-react-mathjax";

import React from 'react';
const Agents = () =>{


  return(
    <div className="App">

      <div className="page">
      <div className="separation">
        <text>Agents specification, hypothesis and critiques for <text id="logo">WoW<text id="cyan">.ai</text></text></text>
      </div>
        <img src={icon1} className="bottom-right-img icon1" alt="WorkOnWork icon with smiling emojis" />
        <MathJaxContext>
          <div className="agents-div">

          For each agents, a set of characterestics (properties, with the formula of how they are updated if necessary) and methods (the actions of the agents) are given.
          <br/> Along with it, we give several <span className="hypothesis"> hypothesis / descriptions</span> with the goal of improvement of the understanding, and <span className="critique">personal critiques / notes for the model evolution</span> in order to better see the model in the long-term.
          <br/><br/>
            <details>
               <summary>Worker</summary>
                <table className="agent">
                   <tr>
                     <td>
                        <img src={worker} className="agent-icon" alt="Icon of a worker on a computer" />
                        <h3>Description</h3>
                        The worker is an individual present in the economy to sell their labour to a company. They periodically seek work (weather to get ouf of unemployment or to just get better pay).
                        They are paid a salary each cycle and get to spend it on consumption goods and keep some as savings.
                        Their skill (and thus productivity) increases with experience.
                        One worker is represented as <MathJax inline>{'\\(\\ell\\)'}</MathJax> in the mathematical formula.
                        <h3>Characteristics</h3>
                        </td></tr><tr><td>
                        <strong>Productivity at period t :</strong> <MathJax >{'\\[A_{\\ell,t} = \\frac{S_t}{\\bar{S_{tot,t}}} * A_{tech,t}\\]'}</MathJax>
                        <br/><i>where</i> &nbsp; <MathJax inline>{'\\(\\bar{S_{tot}}\\)'}</MathJax> is the average overall skill level, A_tech the standard productivity of
                           the technology with which the worker deal (so it is a mean standardized with the number of workers: A_tech = sum(productivity of
                           technologies in capital stock)/number of worker in firm j), and k_j the complexity of the produced good.
                        </td><td className="hypothesis">
                        Productivity is indexed to the compounding contribution of the workers, weighted by their individual skill. A worker's skill affects their efficiency at using a machine.
                        </td></tr>
                        {/*<tr>*/}
                        {/*   <td></td>*/}
                        {/*   <td className="critique">*/}
                        {/*      My critique about productivity*/}
                        {/*   </td>*/}
                        {/*</tr>*/}
                        <tr><td>
                        <strong>Consumption</strong> <MathJax >{'\\[C_\\ell = w_{\\ell} - s_{\\ell}\\]'}</MathJax>, is the amount of money of the worker income dedicated to her consumption.
                        </td><td className="hypothesis">
                        The consumption is according to the income and not the savings capital, that's why in the model their is only flows and no stocks of money.
                        </td></tr><tr><td>
                        <strong>Savings:</strong> <MathJax >{'\\[s_\\ell = \\frac{w_\\ell}{w_{min}}*\\frac{q_1}{100}\\]'}</MathJax> if <MathJax inline>{'\\(w_\\ell < q_5/q_1*w_{min} \\)'}</MathJax>, otherwise <MathJax>{'\\[s_\\ell = \\frac{w_\\ell}{w_{min}}*\\frac{q_5}{100}\\]'}</MathJax>
                        <br/><i>where</i> &nbsp; <MathJax inline>{'\\((q_1, q_5) = (3, 9)\\)'}</MathJax> are numbers representing the quantile of income coefficients in which the worker is situated among all the others.
                        </td><td className="hypothesis">
                        The savings are on income, it is a portion of income that is saved by the worker every period and do not represent in any way the savings capital of the worker.
                        </td></tr><tr><td></td><td className="critique">
                        The function was made quickly to have a complete economic model. However, we know that there are strong hypothesis on this function and also that it is difficult to calibrate it without getting negative values. We should then go toward the full consumption model of what already exists with industial differenciation (luxury and basic-needs goods) to have a more precise set of functions for consumption and savings.
                        </td></tr>
                        <tr>
                           <td>
                              <strong>Skills </strong> <MathJax inline>{'\\(S_t \\)'}</MathJax> the skill of the worker.
                           </td>
                        </tr>
                        <tr>
                           <td>
                              <strong>Wage </strong> <MathJax inline>{'\\(w_l \\)'}</MathJax> of the worker.
                           </td>
                        </tr>
                        <tr><td>

                        <h3>Methods</h3>

                     </td>
                     </tr>
                     <tr>
                        <td>
                           <strong>Skill evolution : </strong>
                           <MathJax >{'\\[S_t = (1 + T_t) * S_{t-1}\\]'}</MathJax> if employed in <MathJax inline>{'\\(t-1\\)'}</MathJax>,
                           <MathJax >{'\\[S_t = (1 + T_u) * S_{t-1}\\]'}</MathJax> otherwise.
                           <br/><i>with</i> &nbsp;<MathJax inline>{'\\(T_t\\)'}</MathJax> the skill accumulation rate on
                           tenure, <MathJax inline>{'\\(T_u\\)'}</MathJax> the skill deterioration rate on unemployment (both equal to 0.01 for now).
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <strong>Adapt Skills To Firm : </strong>
                           <MathJax >{'\\[S_t = min\\{ S \\in L(i)\\}\\]'}</MathJax> if employed in <MathJax inline>{'\\(t-1\\)'}</MathJax>.
                           <br/><i>with</i> &nbsp;<MathJax inline>{'\\(L(i)\\)'}</MathJax> the set of workers of the firm i.
                           Skills are renewed everytime a worker is hired by a new firm
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <strong>Send an application </strong> to a company.
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <strong>Scan offers </strong> to see if a better position is available.
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <strong>Accept an offer </strong> made by a company.
                        </td>
                     </tr>
                  </table>
            </details>
            <details>
               <summary>Capital-good firm</summary>
                  <table className="agent">
                     <tr>
                     <td>
                        <img src={capFirm} className="agent-icon" alt="Icon of a company" />
                        <h3>Description</h3>
                        The capital-good firm is a firm type that innovates on and produces technology, <i>i.e.</i> machine-tools that will be sold on the capital (technology) market. <br /><br />

                        Unlike the consumption-good firm, the capital-good firm agent has a part of its workforce that is dedicated to to technology production, and another that is dedicated to the research and development (R&D). The latter is also divided into a part that innovate, and another that imitate technologies from its close concurrent. Like consumption-good firm, it implements the job-search phase, but the good demand doesn't come from the workers (individuals) but from the consumption-good firms that order and buy it a set of technologies for them to increase their productivity and satisfy their goods demand.<br /> <br />

                        The capital-goods firms have a large set of characteristics and methods that are shared with the consumption-goods firms and that are not reproduced here (Past sales, the productivity, the labour market phase.)

                        One capital-good firm is noted as <MathJax inline>{'\\(i\\)'}</MathJax> in the mathematical formula.
                        <h3>Characterestics</h3>
                        </td></tr><tr><td>
                        <strong>Technology:</strong> the capital-good firm produces a specific technology at period <i>t</i> (see Technology agent for the details).<br /><br/>
                        <strong>Number of technologies:</strong> the number of technologies that is produced for the period <i>t</i>. It is computed as follow:
                        <MathJax>{'\\[Number\\ of\\ tech = \\frac{A_i}{B_{tech}}\\]'}</MathJax>
                        <i>where</i> &nbsp; <MathJax inline >{'\\(B_{tech}\\)'}</MathJax> is the productivity needed to produce the technology, and <MathJax inline >{'\\(A_i\\)'}</MathJax> is the aggregate productivity of the firm <MathJax inline >{'\\(i\\)'}</MathJax>.
                        </td><td className="hypothesis">
                        We take the technology production as homogeneous (only one type of technology) whereas a firm can normally produce a large range of different kinds of technologies.
                        </td></tr><tr><td></td><td className="critique">
                        Also, in the model, we didn't implement yet the stock of technologies that the firm can accumulate (now every period the firm reproduce a set of new technologies, even if the previous ones haven't been sold).

                        </td></tr><tr><td>
                        <strong>Technological generation</strong>: if the firm has access to radical generation and if a next generation (in integer number) in the world discovered by general science exists, then the generation of the firm <MathJax inline >{'\\(i\\)'}</MathJax> increases by 1 and all its next technologies produced will have this new generation.
                        </td><td className="critique">
                        Here we don't consider the eventuality of a refusal for the firm to accept to produce the new generation accessed by its R&D team.

                        </td></tr><tr><td>
                        <strong>Savings:</strong> a portion of income that is saved by the worker every period.
                        </td><td className="critique"> The periodic savings doesn't aims at the total savings (the amount of money)
                        </td></tr><tr><td>
                        <strong>Current technological generation:</strong> the generation (or maximum generation when technology stock will be heterogeneous) embodied by the produced technologied.
                        </td></tr><tr><td>
                        <strong>Labour demand dedicated to R&D</strong> (<i>i.e.</i> the wished number of workers): <MathJax inline >{'\\(L_{i,t}^{R\\&D} = \\frac{RD_{i,t}}{w_{i,t}}\\)'}</MathJax>
                        <br/><i>where</i> &nbsp; <MathJax inline >{'\\(RD_{i,t}\\)'}</MathJax> is the R&D investment decided as a fraction <MathJax inline >{'\\(\\nu\\)'}</MathJax> (set as 0.04) of the past sales of the firm.
                        </td></tr><tr><td>
                        <strong>Innovation share of workers:</strong> <MathJax inline >{'\\(IN = \\xi L_{i,t}^{R\\&D}\\)'}</MathJax>
                        <br/><i>where</i> &nbsp; <MathJax inline >{'\\(\\xi\\)'}</MathJax> is a fixed parameter representing the share of R&D expenditure in imitation (set as 0.5), and <MathJax inline >{'\\(L_{i,t}^{R\\&D}\\)'}</MathJax> is the labour demand dedicated to R&D.
                        </td></tr><tr><td>
                        <strong>Imitation share of workers:</strong> <MathJax inline >{'\\(IN = (1-\\xi) L_{i,t}^{R\\&D}\\)'}</MathJax>
                        </td></tr><tr><td>
                        <strong>Demand in technology:</strong> set according to the consumption-good firm agents.


                        <h3>Methods</h3>
                        </td></tr><tr><td>
                        <strong>Access to incremental innovation</strong>: <MathJax>{'\\[AIN = RAND \\sim Bernouilli(1-e^{-\\xi_1 * IN\'})\\]'}</MathJax>
                        <br/><i>where</i> &nbsp; IN' is the normalized shared of innovative workers (the proportion), and <MathJax inline >{'\\(\\xi_1\\)'}</MathJax> is the search capabilities for innovation (set as 0.100).
                        </td></tr><tr><td>
                        <strong>Access to imitation</strong>: <MathJax inline>{'\\(AIM = RAND \\sim Bernouilli(1-e^{-\\xi_2 * IN\'})\\)'}</MathJax>
                        <br/><i>where</i> &nbsp; IM' is the normalized shared of imitative workers (the proportion), and <MathJax inline >{'\\(\\xi_2\\)'}</MathJax> is the search capabilities for imitation (set as 0.100).
                        </td></tr><tr><td>
                        <strong>Access to radical innovation</strong>: <MathJax inline >{'\\(ARIN = RAND \\sim Bernouilli(1-e^{-\\xi_0 * IN\'})\\)'}</MathJax>
                        <br/><i>where</i> &nbsp; IN' is the normalized shared of innovative workers (the proportion), and <MathJax inline >{'\\(\\xi_0\\)'}</MathJax> is the in-firm access to the newest generation discovered in the scientific litterature (set as 0.020),
                        </td></tr><tr><td>
                        <strong>Innovate incrementally</strong>: If firm's succeeding in accessing a incremental innovation, then its technology labour productivity and its technology needed labour productivity <MathJax inline>{'\\((A_{tech,t}, B_{tech,t}) = (A_{tech,t-1}*(1+x_A), B_{tech,t-1}*(1+x_B))\\)'}</MathJax>
                        <br/><i>where</i> &nbsp; <MathJax inline >{'\\((x_A, x_B)\\)'}</MathJax> are randomly chosen from a beta distribution <MathJax inline >{'\\(\\beta(\\alpha_1, \\alpha_2)\\)'}</MathJax> (both parameters set as 3)  over the fixed support <MathJax inline >{'\\([-0.15,0.15]\\)'}</MathJax> for normalisation.
                        </td></tr><tr><td>
                        <strong>Innovate radically</strong>: If firm's succeeding in accessing a radical innovation, then its technology labour productivity and its technology needed labour productivity <MathJax inline >{'\\((A_{tech,t}, B_{tech,t}) = (A_{g,t}, B_{g,t})\\)'}</MathJax>
                        <br/><i>where</i> &nbsp; <MathJax >{'\\[A_{g,t} = RAND \\sim U(A_{tech,t-1}, (1+h)*A_{tech,t-1}))\\]'}</MathJax> is the productivity of the new technology generation but specific to the firm, <MathJax>{'\\[A_{g,t} = RAND \\sim U(\\frac{A_{tech,t-1}*B_{tech,t-1}}{A_{g,t}}, (1+h)*\\frac{A_{tech,t-1}*B_{tech,t-1}}{A_{g,t}})\\]'}</MathJax> is the new generation labour productivity needed to build the technology, both properties comes from the global scientific development specific to the firm because of the R&D capacity to integrate this new generation of tehcnology to the firm's case.
                        </td></tr><tr><td>
                        <strong>Imitate</strong>: If firm's succeeding in accessing imitation, then its technology labour productivity and its technology needed labour productivity <MathJax inline >{'\\((A_{tech,t}, B_{tech,t})\\)'}</MathJax> are replaced with the ones of a close competitor (if they are superior).
                        <br/><i>where</i> &nbsp; <MathJax inline >{'\\((x_A, x_B)\\)'}</MathJax> are randomly chosen from a beta distribution <MathJax inline >{'\\(\\beta(\\alpha_1, \\alpha_2)\\)'}</MathJax> (both parameters set as 3)  over the fixed support <MathJax inline >{'\\([-0.15,0.15]\\)'}</MathJax> for normalisation.
                        </td><td className="critique">
                        We didn't applied the <i>Dosi et. al.</i> rule of choice over the stock of technology of the competitor as there is no heterogeneous technologies yet.
                        </td></tr><tr><td>
                        <strong>Split workforce</strong> according to the defined shares (R&D, innovation, imitation).
                        </td></tr><tr><td>
                        <strong>Produce technology</strong> according to the defined shares (R&D, innovation, imitation).
                        </td></tr><tr><td>
                        <strong>Receive orders</strong> of technologies from the consumption good-firm.
                        </td></tr><tr><td>
                        <strong>Deliver technologies</strong> to the consumption good-firm.


                     </td>
                  </tr>
                  </table>
            </details>
            <details>
               <summary>Consumption-good firm</summary>
                  <table className="agent">
                     <tr>
                     <td>
                        <img src={consFirm} className="agent-icon" alt="Icon of a company" />
                        <h3>Description</h3>
                        The consumption-good firm is the standard type of company that creates products directly for the consumer market. All of its workforce is dedicated to contributing
                        to the production of the consumer goods. To continuously improve its productivity, the consumption-good firm orders new technology from a capital-good firm.
                        Each cycle, consumption good firms will accept job applications and accept a set number of new workers based on an expected production output on the next cycle.

                        Consumption-good firms are noted as <MathJax inline>{'\\(j\\)'}</MathJax> in mathematical formulas.
                        <h3>Characteristics</h3>
                        </td></tr>
                     <tr>
                        <td>
                        <strong>Past sales:</strong> <MathJax inline>{'\\(S^-\\)'}</MathJax>. A record of the sales of the firm in previous cycles.
                        </td>
                     </tr>
                     <tr>
                        <td>
                        <strong>Firm effective productivity:</strong> <MathJax inline>{'\\[FEP(t) = \\frac{1}{L} * \\sum_1^L A(t, l)\\]'}</MathJax>
                     where <MathJax inline>{'\\(L\\)'}</MathJax> is the set of workers of the firm, and A(t,l) is the productivity of worker l at time t.</td>
                     </tr>
                     <tr>
                        <td>
                           <strong>Labour Demand </strong> <MathJax inline>{'\\(LD = \\frac{Q}{A}\\)'}</MathJax> where  <MathJax inline>{'\\(Q\\)'}</MathJax> is
                           the desired production and A the expected labour productivity (calculated with the firm effective productivity I guess).
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <strong>Desired production </strong> <MathJax inline>{'\\(Q\\)'}</MathJax>, expected demand with inventories not taken into account).
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <strong>Expected demand </strong> <MathJax >{'\\[D_e(t) = mean\\{D(t-1), D(t-2), D(t-3)\\} \\]'}</MathJax> where <MathJax inline>{'\\(D(t)\\)'}</MathJax> is
                           the actual demand (if <MathJax inline>{'\\(D\\)'}</MathJax> exists at <MathJax inline>{'\\(t-x\\)'}</MathJax> for the first periods).
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <strong>Actual demand </strong> <MathJax inline>{'\\(D(t)\\)'}</MathJax>, determined by worker consumption.
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <strong>Expected Labour Supply  </strong> <MathJax inline>{'\\(E[LS]\\)'}</MathJax>. <MathJax >{'\\[E[LS] = [w(1-u(t-1)) + w_u * u(t-1)]*L_S * f(j, t-1)\\]'}</MathJax>
                           where <MathJax inline>{'\\(w, w_u\\)'}</MathJax> are parameters that defines the number of application a worker send (weather if employed or not, defined
                           as <MathJax inline>{'\\(w=5\\)'}</MathJax> and <MathJax inline>{'\\(w_u = 10\\)'}</MathJax>), <MathJax inline>{'\\(u(t-1)\\)'}</MathJax> the unemployment
                           rate, <MathJax inline>{'\\(L_S\\)'}</MathJax> the fixed total labour supply, and <MathJax inline>{'\\(f(j,t-1)\\)'}</MathJax> the market share of the firm <MathJax inline>{'\\(j\\)'}</MathJax>.
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <strong>Capital Stock </strong> <MathJax inline>{'\\(K(j, t)\\)'}</MathJax>, set of technologies, with all their properties.
                        </td>
                     </tr>
                     <tr>
                        <td>
                        <h3>Methods</h3>
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <strong>Desired Capital Stock in productivity :</strong> <MathJax >{'\\[DSCProd = (FEP – Q) * \\frac{|L|}{\\sum_L s_\\ell/(\\bar{s} * card\\{\L\\}}\\]'}</MathJax> where L is
                           the set of workers of the firm, and A(t,l) is the productivity of worker l at time t. It is the sum of the technologies' productivity needed in addition to the current
                           set of technologies productivity to accomplish the desired production output.
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <strong>Desired Capital Stock in price :</strong> <MathJax >{'\\[DCS = 0.5*(\\frac{S}{RPC})*Q \\]'}</MathJax> where <MathJax inline>{'\\(S\\)'}</MathJax> are past sales.
                        </td>
                     </tr>
                     <tr>
                        <td>
                        <strong>Rental price of capital :</strong>  <MathJax >{'\\[RPC = delta * E[P(new machine)]\\]'}</MathJax> where <MathJax inline>{'\\(E[P(new machine)]\\)'}</MathJax> is
                           the average price of the new machines in the catalog, and delta is the depreciation rate of capital set as 0.15.
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <strong>Invest : </strong> <MathJax >{'\\[I = DSC - \\sum ((tech(j).p)\\]'}</MathJax>, the amount the firm can use to buy new technologies. The firm replace
                           the current technologies tech(j) that have: <MathJax >{'\\[p*(i)/(PC(j)-PC*(i) <= b\\]'}</MathJax> for each new technologies, with payback
                           parameter <MathJax inline>{'\\(b\\)'}</MathJax>, where <MathJax inline>{'\\(p*(i)\\)'}</MathJax> and <MathJax inline>{'\\(PC*(i)\\)'}</MathJax> are
                           the price and production cost of the new technologies in the catalogs.
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <strong>Order Technologies : </strong> if the company can’t produce the desired output <MathJax inline>{'\\(Q\\)'}</MathJax>, then it will order new
                           machines. Giving the past sales and an additional fraction of them that is a loan to the banks (minus an interest rate), the firm will choose among the possible better
                           technologies (from all capital-good sector).
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <strong>Sort Technologies Catalogs</strong> to decide the best one to choose, and the minimum required to produce output.
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <strong>Choose Technologies To Order</strong>.
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <strong>Should Invest</strong> : <MathJax >{'\\[DCS > \\sum (tech.p)\\]'}</MathJax>. If DCS is higher than sum of prices of technologies then true else false
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <strong>Labour market phase : </strong>
                           <ul>
                              <li>
                                 <strong>Select Candidates : </strong> workers with a requested wage <MathJax inline>{'\\(w_r(l, t)\\)'}</MathJax> is below offered
                                 wage <MathJax inline>{'\\(w_o(j,t)\\)'}</MathJax> of firm are hired, up to Labour Demand, or up to the candidates number.
                                 The <MathJax inline>{'\\(w_r\\)'}</MathJax> are per worker at time <MathJax inline>{'\\(t\\)'}</MathJax>, and <MathJax inline>{'\\(w_o\\)'}</MathJax> is a unique
                                 offered wage from firm <MathJax inline>{'\\(j\\)'}</MathJax> at time <MathJax inline>{'\\(t\\)'}</MathJax>.
                              </li>
                              <br/>
                              <li>
                                 <strong>Update Offered Wage : </strong> <MathJax >{'\\[w(j,t) = [1 + WP(j,t) + RAND ~ N(0,w_{err}) ] * w(j, t-1)\\]'}</MathJax> where <MathJax inline>{'\\(WP\\)'}</MathJax> is
                                 the wage premium, <MathJax inline>{'\\(w_{err}\\)'}</MathJax> is the standard deviation of the error when evaluating the market wage,
                                 and <MathJax inline>{'\\(w(j,t)\\)'}</MathJax> is lower or equal than (price of sold product in the firm <MathJax inline>{'\\(j\\)'}</MathJax>) * (aggregate labour
                                 productivity of firm <MathJax inline>{'\\(j\\)'}</MathJax>), that is to say the case in which there will be 0 profit, and higher or equal to government minimum wage
                                 (see government agent).
                              </li>
                              <br/>
                              <li>
                                 <strong>Offered Wage Premium : </strong> <MathJax >{'\\[WP = psi_2(\\Delta A(t)/A(t-1)) + psi_4(\\Delta A(j,t)/A(j,t-1))\\]'}</MathJax> where <MathJax inline>{'\\(psi_2\\)'}</MathJax> is
                                 the aggregate productivity pass-through (set as 1), <MathJax inline>{'\\(psi4\\)'}</MathJax> is the firm-level productivity pass-through (set as 0.5),
                                 and <MathJax inline>{'\\(A(t)\\)'}</MathJax> is the aggregate labour productivity (in-firm or not).
                              </li>
                              <br/>
                              <li>
                                 <strong>Give Bonus To Worker : </strong> <MathJax >{'\\[B_l = = [ psi_6*(1-tr) PI(j,t-1) ] / N_l\\]'}</MathJax> where <MathJax inline>{'\\(psi_6\\)'}</MathJax> is
                                 the share of firm free cash flow paid as bonus (set as 0.2), <MathJax inline>{'\\(PI\\)'}</MathJax> the firm gross profit.
                              </li>
                           </ul>
                        </td><td className="hypothesis">
                        The aggregate supply of labour is fixed and all workers are available to be hired in any period.
                        <br/>
                        <br/>
                        A crucial assumption here is that labour wage is indexed to productivity. This highlights the strong relation between
                        labour and capital and allows us to put forward the problematics of this project.
                     </td>
                     </tr><tr><td>


                     </td>
                  </tr>
                  </table>
            </details>
            <details>
               <summary>Technology</summary>
               <table className="agent">
                  <tr>
                     <td>
                        <img src={tech} className="agent-icon" alt="Icon of a robotic arm" />
                        <h3>Description</h3>
                        The technology is more a structure, an object, than an agent in our model. It is indeed not interacting by its own action but rather by its properties. Its main utility here is to be sold and bought by the capital-good sector to the consumption-good one, and to change the worker productivity.

                        <br/><br/>s It affects the worker productivity by increasing it (see worker productivity formula). So the technology has no clear effect on whether the worker will stay in the firm or whether it will be considered as too productive along with the machine-tools to avoid to hire other worker.
                        One technology is noted as <MathJax inline >{'\\(tech\\)'}</MathJax> in the mathematical formula.
                        <h3>Characterestics</h3>
                        </td></tr><tr><td>
                        <strong>Labour productivity of the technology</strong> <MathJax inline >{'\\(A_{tech}\\)'}</MathJax><br /><br/>
                        <strong>Labour productivity needed to build the technology</strong> <MathJax inline >{'\\(B_{tech}\\)'}</MathJax><br/><br/>
                        <strong>Production cost</strong> <MathJax inline >{'\\(c_{tech,t} = \\frac{w_{j,t}}{B_{tech,t}}\\)'}</MathJax>
                        </td></tr><tr><td>
                        <strong>Price</strong> <MathJax inline >{'\\(p_{tech,t} = (1+\\mu_1)*c_{tech,t}\\)'}</MathJax>
                        <br/><i>where</i> &nbsp; <MathJax inline >{'\\(\\mu_1\\)'}</MathJax> is a mark-up set as 0.100 capturing the firm's profits.
                        </td><td className="critique">
                        The mark-up could be here a more complex function of the firm j's market share and lonelinesss in the market (not possible due to the lack of technology heterogeneity here), to capture the keynnesian's corporate superprofit as we're seeing now over company's size.
                        </td></tr><tr><td>
                        <strong>Generation</strong>: to keep track of the generations acquired by the technological companies in the consumption-good firms and for the aggregated variabes.

                        </td><td className="hypothesis">
                        The generation describe an evolutionnary processus as <i>Dosi et. al.</i> are used to work with. It doesn't capture the heteroogeneity of the innovation but rather the change in productivity and the impact is has on industries.
                        </td></tr><tr><td>
                        <strong>Disruptive</strong>: if true, then the technology is a new generation from the firm at period t.
                        </td><td className="hypothesis">
                        In this evolutionnary model, a disruptive technology will affect the complexity of the produced good in the consumption-good sector, allowing the firm it is using it to have a more expensive product, that will be of a luxury type (long-lasting, different use from the consumer).

                     </td>
                   </tr>

                </table>
            </details>
            <details>
               <summary>Coordinator / World</summary>
                <table className="agent">
                   <tr>
                     <td>
                        <img src={world} className="agent-icon" alt="Icon of a planet" />
                        <h3>Description</h3>
                        The coordinator agent is a agent that has more a computer engineering interest than a economic meaning. Indeed, it is useful to manage agents, steps in each cycle, and also to run each simulations. Thanks to it we can compute aggregated variables.<br/><br/>
                        We thus represented it here in this page to show more deeply how the agent-based model works in our case.
                        <h3>Characterestics</h3>
                        </td></tr><tr><td>
                        <strong>Agents</strong>: the coordinator possesses all the agents to be able to manipulate them and make them interacts, exchange things between everyone if those exchanges are not necessarily random (as, in our model, the distribution of the catalogs of technologies from capital-good firms to the consumption-good ones are made by the coordinator).
                        <ul>
                        <li>Workers</li>
                        <li>Consumption-good firms</li>
                        <li>Capital-good firms</li>
                        <li>Government</li>
                        <li>Technologies</li>
                        </ul>
                        </td><td className="critique">
                        The fact that there is a coordinator in our model is definitely altering the agents that should undertake asynchronous interactions as in real life. But our current model is done makng sequences of actions, that  is to say that our agents change the sequence synchronously. To avoid that situation, we should think to a non-sequential model...<br/>
                        Also, the agent still interact between them asynchronously inside the sequences itself (as for the labour market and the capital (technologies) market.
                        </td></tr><tr><td>
                        <strong>Aggregate variable</strong>: a series of computations of aggregated variable over all agents, facilitated by the use of this coordinator:
                        <ul>
                        <li>Minimum, maximum, average, median wages</li>
                        <li>Aggregate computations on technological generation</li>
                        <li>Aggregate computations on technologies production</li>
                        <li>Employment</li>
                        <li>Technologies</li>
                        <li>Aggregate labour production</li>
                        <li>Aggregate consumption</li>
                        <li>Technologies</li>
                        <li>Aggregate wage</li>
                        <li>Aggregate income over capital-good and consumption-good sector</li>
                        <li>(the list could evolve)</li>
                        </ul>
                        </td></tr><tr><td>
                        <strong>Period (epoch)</strong>: the current period the agent-based model is in. It starts at 0 and for our first simulations, we ended it at 100.
                        </td><td className="hypothesis">
                        The period consistency with real time is dependant on calibration. For instance, if we recognize some patterns in the simulation that corresponds to real life ones, then we can associate the period with a real time.<br/>
                        In the <i>Dosi et. al.</i>'s paper, as they set a retreat for the workers after 120 periods, we guess that a period can correspond with a real time of between <strong>3 or 4 months</strong>.
                        </td></tr><tr><td>








                        <h3>Methods</h3>
                        </td></tr><tr><td>
                        <strong>Access to new technological innovation</strong>: <MathJax>{'\\[ANG = RAND \\sim Bernouilli(1-e^{-\\xi_g * g(Ag_{t-1}, Ag_{t-2}, Ag_{t-3}) })\\]'}</MathJax>
                        <br/><i>where</i> &nbsp; IN' is the normalized shared of innovative workers (the proportion), and <MathJax inline >{'\\(\\xi_g\\)'}</MathJax> is the emergence (not in-firm! at the global scientific level) probability that a new technology arises in any period t (set as 0.030),
                        and and g a moving average function of the aggregate productivity of the world over 3 periods.
                        </td><td className="critique">Same remark as in firms' acces to innovation/imitation: the evolutionnary assumption of stochastic emergence could be more explained to be consistent with the real world data.<br/>
                        In this case, also, the emergence could have an even wider variance to model several innovation as seen in the industrial revolution or since the information revolution,
                        that is another level of radical innovation than "smaller" radical improvement in machine learning. <br/>
                        Last, but not least, a radical innovation of upper generation would have no really other effect than another level of productivity. In the complete <i>Dosi et. al.</i>'s model, a radical innovation
                        open a new market changing a product complexity and then its market valuation. A further step would be to integrate the societal change other than industries and products change,
                        or even the vulnerabilities that could emerge from this scientific process as seen on <a href="https://nickbostrom.com/papers/vulnerable.pdf" target="_blank">Bostrom, 2019</a> paper (do not rely on everything for the project). <br/>
                        We then can understand that this formula cannot capture all effects, even if the stochastic approach can be really interesting if improved.
                        </td></tr><tr><td>
                        <strong>Make all capital-good firms send catalog to all consumption-good firms</strong>
                        </td></tr><tr><td>
                        <strong>Consume</strong>: make the worker consume the produced goods from firm. A worker chooses one firm chosen randomly accordingly to its size but not perfectly (imperfect market formation), <i>i.e.</i>, there is a small part of hazard during the choice of the worker.
                        </td><td className="critique">The consumption should be across several firms and, as the economic models already show, according to the different types of goods: the luxury ones and the basic-needs ones.<br/>
                        This dichotomy between the two are because of their fundamental difference of price-elasticity of the demand.
                        The basic-needs are bought whether the individual have the buying power to get it, while the luxury one is bought according to the revenues<br/>
                        This could make a verification on the <a href="https://en.wikipedia.org/wiki/Engel%27s_law" target="_blank">Engel law</a>.





                     </td>
                   </tr>

                </table>
            </details>
            <details>
               <summary>Government</summary>
                <table className="agent">
                   <tr>
                     <td>
                        <img src={gov} className="agent-icon" alt="Icon of a government building" />
                        <h3>Description</h3>
                        </td></tr><tr><td>
                        The government is for now a lonely agent that provide the necessary metrics coming from the state (minimum wage, unemployment benefits, if there is...).
                        </td><td className="critique">
                        It now could have been integrated with the coordinator agent as it doesn't have other interactions than computing aggregate variable in addition of some calculations, but we decided to create a complete agent of it in the model to make it easyer with the evolution of the model.
                        Indeed, it will after serveral updates of the agent-based model incorporate a certain budget, and for example will interact through banks (financial markets).
                        </td></tr><tr><td>
                        <h3>Characterestics</h3>
                        </td></tr><tr><td>
                        <strong>Minimum wage:</strong> <MathJax inline >{'\\(w_{min,t} = w_{min,t-1}*(1+\\psi_2*\\frac{\\Delta A_{agg,t}}{A_{agg,t-1}})\\)'}</MathJax> is indexed on aggregated labour productivity <MathJax inline >{'\\(A_{agg}\\)'}</MathJax> <br />
                        <i>where</i> &nbsp; <MathJax inline >{'\\(\\psi_2\\)'}</MathJax> is the aggregate productivity pass-through set as 1.
                        </td><td className="hypothesis">
                        The model then allow us to abandonned the only idea on wage set according to the market.
                        </td></tr><tr><td>
                        <strong>Unemployment benefits:</strong> <MathJax inline >{'\\(w_{u,t} = \\psi*\\bar{w}_{u,t-1}\\)'}</MathJax> is a fraction of the current average wage.
                        <br/><i>where</i> &nbsp; <MathJax inline >{'\\(\\psi\\)'}</MathJax> is the fraction parameter set as 0.4.
                        </td></tr><tr><td></td><td className="critique">
                        The formula is perhaps too optimistic knowing that the real wages are not indexed to the average wage. This formula doesn't capture the inequality present in our current society through wages (primary repartition).
                        </td></tr><tr><td>
                        <strong>Tax rate:</strong> set as 0.1 for the moment.
                        </td><td className="critique">
                        Not used yet in our current version of the model, it could have been computed in a way such that it embody a current vision of the government (with neoliberalism the corportate tax rate structurally tend to be lower for example.)
                        </td></tr><tr><td>
                     </td>
                   </tr>
                </table>
            </details>
          </div>
        </MathJaxContext>
    </div>
  </div>
  );
}
export default Agents;
