import React, {useState, useEffect} from 'react';
import Plot from 'react-plotly.js';
import axios from 'axios';
import interactions from '../assets/img/agent_interactions.png'
import animatedGears from '../assets/img/anim_gears.gif'
import gears from  '../assets/img/gears.gif'





const Simulation = () =>{
  const initObject = {
    // Here is defined the env variable that will update while fecthing Data
    // Except epoch, every variables will be
    // All these properties, except "x" should be specified in the Go API.
    epoch: -1,
    axis: [],   //for X axis
    unemployment: [],
    average_wage: [],
    median_wage: [],
    maximum_wage: [],
    minimum_wage: [],
    created_technologies_count: [],
    maximum_tech_generation: [],
    average_tech_generation: [],
    minimum_tech_generation: [],
    highest_tech_generation_emerged: [],
    technology_firm_income: [],
    con_firms_income: [],
    cap_firms_income: [],
    firms_income: [],
    unemployment: [],
    created_technologies_count: [],
    tech_sold: []
  }
  // Define useState updatable object of ABM variables
  const [env, setEnv] = useState(initObject);
  var envcop = {}
  var startButton;


  // fetch data from API
  useEffect(() => {
    console.log("UseEffect")
    console.log(env);
    async function fetchEnv() {
      try{
        // Set up an listener for POST requests
        console.log("Fetch environment data of simulation from the API");
        const response = await fetch('http://localhost:8080/epoch_done', { method: 'GET' });
        const data = await response.json();
        // data properties are number while tmp and env propoerties are ARRAYS of number in which we push the data number
        const tmp=Object.assign({}, env);
        if (data.epoch !== env.epoch && data.epoch != -1) {
          tmp.epoch = data.epoch;
          for (const key of Object.keys(env)) {
            console.log(key);
            if (key === "axis") {
              // handle the axis
              tmp.axis = [...tmp.axis,data.epoch];
            }
            else if (key === "active") {
              if (data.active === "false") {
                setStartResponse(false)
              }
            }
            else if (key !== "epoch") {
              tmp[key] = [...tmp[key], data[key]];
            }
          //  console.log(tmp["unemployment"];
          }
          setEnv(tmp);
          console.log(env);
          envcop = Object.assign({}, tmp)
          console.log(tmp.axis);
        }
      } catch(e) {
        console.log("No response from epoch_done route of API.")
      }

    };
    console.log(env.epoch)
    var interval = setInterval(() => {fetchEnv()}, 1500)
    console.log(env.epoch)
    return () => clearInterval(interval)
    console.log(env.epoch)
  }, [env]);

  const [startResponse, setStartResponse] = useState(false);
  const [startError, setStartError] = useState(null);
  const handleStart = () => {
    setEnv(env => initObject)
    axios.post('http://localhost:8080/simulate', {
      // data to send in the POST request if any
    })
      .then(res => {
        // handle the response
        console.log("Response received : " + res)
        setStartError(false)
        setStartResponse(true);
      })
      .catch(err => {
        // handle the error
        setStartError(true)
        setStartResponse(false)
        console.error("No response from API for starting simulation.");
      });
  }

  const handleStop = () => {
    axios.post('http://localhost:8080/stop', {
      // data to send in the POST request if any
    })
      .then(res => {
        // handle the response
        console.log("Response received : " + res)
        setStartError(false)
        setStartResponse(false);
      })
      .catch(err => {
        // handle the error
        setStartError(true)
        setStartResponse(true)
        console.error("No response from API for stopping simulation.");
      });

  }


  return (
    <div class="page">
      <h1>Simulation</h1>
      <div>
        <button onClick={handleStart} className="startbutton" disabled={startResponse ? true : false}>Start simulation
        </button>
      </div>
      <div>
        <button onClick={handleStop} className="stopbutton" disabled={startResponse ? false : true}>Stop simulation
        </button>
      </div>
      <div>
      {startError ? (<div style={{color: "red"}}> No answer from Go API, don't forget to start the server app.</div>) : (<div></div>)}
      <br>
      </br>
      </div>
      <p>Current simulation epoch: {env.epoch == -1 ? 0 : env.epoch}</p>
      <h3>Graphs</h3>

      <div>
        A set of time series of interest variables plotted in real-time along with the simulation.
      </div>
      <div></div>
      <Plot
        graphDiv="wages"

        data={[
          {
            x: env.axis,
            y: env.average_wage,
            name: "Average wage",
            type: 'scatter',
            line: {color: '#7F7F7F'}
          },
          {
            x: env.axis,
            y: env.median_wage,
            name: "Median wage",
            type: 'scatter',
            line: {color: '#17BECF'}
          },
          {
            x: env.axis,
            y: env.minimum_wage,
            name: "Minimum wage",
            type: 'scatter',
            line: {color: '#1AB93F'}
          }
        ]}

          layout={ {

          title: 'Wages overview',
          xaxis: {
            autorange: true,
            rangeslider: {},
            type: 'linear'
          },
          yaxis: {
            autorange: true,
            type: 'linear'
          }

        } }

      />

      <Plot
        graphDiv="firms_income"

        data={[
          {
            x: env.axis,
            y: env.cap_firms_income,
            name: "Capital-good firms",
            type: 'scatter',
            line: {color: '#7F7F7F'}
          },
          {
            x: env.axis,
            y: env.con_firms_income,
            name: "Consumption-good firms",
            type: 'scatter',
            line: {color: '#17BECF'}
          },
          {
            x: env.axis,
            y: env.firms_income,
            name: "All firms",
            type: 'scatter',
            line: {color: '#1AB93F'}
          }
        ]}

          layout={ {

          title: 'Aggregate income of firms',
          xaxis: {
            autorange: true,
            rangeslider: {},
            type: 'linear'
          },
          yaxis: {
            autorange: true,
            type: 'linear'
          }

        } }

      />
      <Plot
        graphDiv="tech_generation"

        data={[
          {
            x: env.axis,
            y: env.created_technologies_count,
            name: "Total created",
            type: 'scatter',
            line: {color: '#1AB93F'}
          },
          {
            x: env.axis,
            y: env.tech_sold,
            name: "Total sold",
            type: 'scatter',
            line: {color: '#19a4cc'}
          }
        ]}

          layout={ {

          title: 'Technologies in market overview',
          xaxis: {
            autorange: true,
            rangeslider: {},
            type: 'linear'
          },
          yaxis: {
            autorange: true,
            type: 'linear'
          }

        } }

      />
      <Plot
        graphDiv="tech_generation"

        data={[
          {
            x: env.axis,
            y: env.average_tech_generation,
            name: "Average on market",
            type: 'scatter',
            line: {color: '#7F7F7F'}
          },
          {
            x: env.axis,
            y: env.maximum_tech_generation,
            name: "Highest on market",
            type: 'scatter',
            line: {color: '#FC2244'}
          },
          {
            x: env.axis,
            y: env.minimum_tech_generation,
            name: "Lowest on market",
            type: 'scatter',
            line: {color: '#1AB93F'}
          },
          {
            x: env.axis,
            y: env.highest_tech_generation_emerged,
            name: "Highest emerged",
            type: 'scatter',
            line: {color: '#19a4cc'}
          }
        ]}

          layout={ {

          title: 'Technology paradigms emerged and on market overview',
          xaxis: {
            autorange: true,
            rangeslider: {},
            type: 'linear'
          },
          yaxis: {
            autorange: true,
            type: 'linear'
          }

        } }

      />

      <Plot

        data={[
          {
            x: env.axis,
            y: env.unemployment,
            type: 'scatter'
          }
        ]}

        layout={ {

        title: 'Unemployment',
        xaxis: {
          autorange: true,
          rangeslider: {},
          type: 'linear'
        },
        yaxis: {
          autorange: true,
          type: 'linear'
        }

      } }

      />

      <h3>Agent interactions static diagram</h3>
        <img src={interactions} className="App-interactions" alt="interaction" style={{"margin-bottom": "15px;", "width": "85%"}} align="absbottom" />

        <img src={startResponse ? animatedGears : gears} className="bottom-right-img gears" alt="Animated gears" />
    </div>



  );
}
export default Simulation;
