package main

import (
	"fmt"
	agt "ia04_wow/agt"
	"ia04_wow/wow"
	"math/rand"
)

//This file is aimed for local debugging, please refer to launch_app.go to start the app.

func main() {
	rand.Seed(42)
	// ABM object from the wow object
	abm := wow.NewABMSkeleton()

	// Adding all agents

	//  Government
	abm.AddGovernment(agt.NewGovernment(abm.Param.InitialUnemploymentBenefit, abm.Param.InitialMinimumWage, abm.Param.TaxRate))
	//	Workers
	for i := 0; i < 5; i++ { //for i := 0; i < abm.Param.NumberOfWorkers; i++ {
		abm.AddWorker(agt.NewWorker(
			abm.Param.MinimumDesiredWageIncreaseRate,
			abm.Param.InitialUnemploymentBenefit,
			abm.Param.InitialSkills,
			abm.Param.InitialAge,
			abm.Param.RetiringAge, false, false))
	}
	//	Firms
	for i := 0; i < 3; i++ { // for i := 0; i < abm.Param.NumberOfConsumptionGoodFirms; i++ {
		abm.AddConFirm(agt.NewConsumptionGoodFirm(
			abm.Param.InitialOfferedWage,
			abm.Param.InitialOfferdWagePremium,
			abm.Param.InitialPastSales,
			abm.Param.InitialMarketShare,
			abm.Param.InitialExpectedProduction))
	}
	for i := 0; i < 2; i++ { //for i := 0; i < abm.Param.NumberOfCapitalGoodFirms; i++ {
		abm.AddCapFirm(agt.NewCapitalGoodFirm(
			abm.Param.InitialOfferedWage,
			abm.Param.InitialOfferdWagePremium,
			abm.Param.InitialPastSales,
			abm.Param.InitialMarketShare,
			abm.Param.InitialExpectedProduction,
			abm.Param.InitialLabourProductivityOfTechnology,
			abm.Param.InitialLabourProductivityNeededToBuildTechnology))
	}

	// Set the desired number of epochs the simulation is going to run for
	numEpochs := abm.Param.NumberOfEpochs
	abm.SetEpochs(numEpochs)

	epochs := make(chan agt.WorldState, numEpochs)
	abm.SetReportingChannel(epochs)
	abm.Active = true // not defined before in launch_simulation, but defined before in launch_app during server response (launchSimulationHandler)

	//	Start the simulation in a separate execution thread

	go abm.Simulate()

	//print out retrieved data
	for i := 0; i < numEpochs; i++ {
		fmt.Println("\n\n\n\n =========================================================\n ========================================================= ")
		fmt.Printf("Epoch number %d\n", <-epochs)
	}
}
