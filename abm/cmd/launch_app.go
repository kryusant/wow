package main

import (
	"fmt"
	agt "ia04_wow/agt"
	"ia04_wow/wow"
	"log"
	"net/http"
	"time"
	"encoding/json"
)

func main() {
	// ABM object declaration from the wow object, needed to handle parameters change before starting simulation.
	abm := wow.NewABMSkeleton()

	// router creation (API)
	mux := http.NewServeMux()
	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS")
		w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
		w.WriteHeader(200)
		fmt.Fprintf(w, "You are in the main endpoint of WoW server API.")
	})
	mux.HandleFunc("/simulate", launchSimulationHandler(abm))
	mux.HandleFunc("/stop", stopSimulationHandler(abm))
	mux.HandleFunc("/epoch_done", sendDataToClient(abm))
	// mux.HandleFunc("/change_param", function)


	// http server creation
	port := ":8080"
	s := &http.Server{
		Addr:           port,
		Handler:        mux,
		ReadTimeout:    4 * time.Second,
		WriteTimeout:   4 * time.Second,
		MaxHeaderBytes: 1 << 20}


	// launch server
	log.Println("Listening on", port)

	go log.Fatal(s.ListenAndServe())

	fmt.Scanln()

}

func sendDataToClient(abm *wow.WorkOnWork) http.HandlerFunc{
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS")
		w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
		w.Header().Set("Content-Type", "application/json")


		if !checkMethod("GET", w, r) {
			w.WriteHeader(501)
			return
		}
		fmt.Println("This report will be sent back to a client: ")


		w.WriteHeader(http.StatusOK)
		serial, _ := json.MarshalIndent(abm.Report, "", "  ");
		fmt.Println(string(serial))

		w.Write(serial)
		// Send every second and a half to answer React app on the abm Report
	}
}

// launchSimulationHandler(): a wrapper function (to have the control over the ABM all the time) that is aims to handle a Go routine of the simulation
func launchSimulationHandler(abm *wow.WorkOnWork) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request){

		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS")
		w.Header().Set("Access-Control-Allow-Headers", "Content-Type")


		if checkMethod("OPTIONS", w, r) {
		 	w.WriteHeader(200)
			fmt.Fprintf(w, "Got requests options.")
		} else {
			if abm.Active == true {
				w.WriteHeader(http.StatusOK)
				fmt.Fprintf(w, "Simulation already in progress.")
			} else {
				w.WriteHeader(http.StatusOK)
				fmt.Fprintf(w, "Starting simulation.")
				go launchSimulation(abm)
				// no need of wait groups as we control the ABM by its pointer to set its Active attribute.
			}
		}

	}
}

// stopSimulationHandler(): set the abm.Active attribute to false in order for the abm to stop at next epoch
func stopSimulationHandler(abm *wow.WorkOnWork) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request){

		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS")
		w.Header().Set("Access-Control-Allow-Headers", "Content-Type")


		if checkMethod("OPTIONS", w, r) {
		 	w.WriteHeader(200)
			fmt.Fprintf(w, "Got requests options.")
		} else {
				w.WriteHeader(http.StatusOK)
				abm.Active = false
				log.Println("Simulation stoppped on POST request.")
				fmt.Fprintf(w, "Simulation stopped.")
		}
	}
}

// launchSimulation(): prepare the state of the simulation, create agents and initialize variables, and start simulation.
func launchSimulation(abm *wow.WorkOnWork) {

 	abm.ResetABMSkeleton() // reset abm at each launch to handle start/stop of the React app
	abm.Active = true // indicate it is active for the React app

	// Adding all agents

	//  Government
	abm.AddGovernment(agt.NewGovernment(abm.Param.InitialUnemploymentBenefit, abm.Param.InitialMinimumWage, abm.Param.TaxRate))
	//	Workers
	for i := 0; i < abm.Param.NumberOfWorkers; i++ {
		abm.AddWorker(agt.NewWorker(
			abm.Param.MinimumDesiredWageIncreaseRate,
			abm.Param.InitialUnemploymentBenefit,
			abm.Param.InitialSkills,
			abm.Param.InitialAge,
			abm.Param.RetiringAge, false, false))
	}
	//	Firms
	for i := 0; i < abm.Param.NumberOfConsumptionGoodFirms; i++ {
		abm.AddConFirm(agt.NewConsumptionGoodFirm(
			abm.Param.InitialOfferedWage,
			abm.Param.InitialOfferdWagePremium,
			abm.Param.InitialPastSales,
			abm.Param.InitialMarketShare,
			abm.Param.InitialExpectedProduction))
	}
	for i := 0; i < abm.Param.NumberOfCapitalGoodFirms; i++ {
		abm.AddCapFirm(agt.NewCapitalGoodFirm(
			abm.Param.InitialOfferedWage,
			abm.Param.InitialOfferdWagePremium,
			abm.Param.InitialPastSales,
			abm.Param.InitialMarketShare,
			abm.Param.InitialExpectedProduction,
			abm.Param.InitialLabourProductivityOfTechnology,
			abm.Param.InitialLabourProductivityNeededToBuildTechnology))
	}

	// Set the desired number of epochs the simulation is going to run for
	numEpochs := abm.Param.NumberOfEpochs
	abm.SetEpochs(numEpochs)

	epochs := make(chan agt.WorldState, numEpochs)
	abm.SetReportingChannel(epochs)

	//	Start the simulation in a separate execution thread

	go abm.Simulate()

	//print out retrieved data
	for i := 0; i < numEpochs; i++ {
		fmt.Println("\n\n\n\n =========================================================\n ========================================================= ")
		fmt.Printf("Epoch number %d\n", <-epochs)
	}
}

func checkMethod(method string, w http.ResponseWriter, r *http.Request) bool {
	if r.Method != method {
		return false
	}
	return true
}

// decodeRequest: Useful function to treat data in request
// func decodeRequest(r *http.Request) (req testjson, err error) {
// 	buf := new(bytes.Buffer)
// 	buf.ReadFrom(r.Body)
// 	err = json.Unmarshal(buf.Bytes(), &req)
// 	return
// }
