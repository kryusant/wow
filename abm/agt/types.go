package agt

type FirmInterface interface {
	Firm
}

type WorkerQuits struct {
	workerID int
	quits    bool
}

type WorkerJobRequest struct {
	workerID        int
	desiredWage     float64
	workerOfferChan chan FirmJobRequest
}

type FirmJobRequest struct {
	firmID         int // <!> Idem
	offeredWage    float64
	firmOfferRChan chan WorkerJobAcceptanceResponse
}

type WorkerJobAcceptanceResponse struct {
	worker     *Worker
	acceptance bool
	quits      bool
}

type Catalog struct {
	Technology   Technology
	FirmProdChan chan Order
}

type Order struct {
	firmID        int
	firmOrderChan chan Technology
}

type WorldState struct {
	Epoch int `json:"epoch"`

	Active bool `json:"active"`

	ConsFirmCount int `json:"cons_firm_count"`
	CapFirmCount  int `json:"cap_firm_count"`

	WorkerCount     int `json:"worker_count"`
	UnemployedCount int `json:"unemployed_count"`

	AggrLabourProd float64 `json:"aggr_labour_prod"`
	//AggrLabourProdByFirm []float64 `json:"aggr_labour_prod_by_firm"`

	Unemployment 			float64 	`json:"unemployment"`

	MinimumWage       float64   `json:"minimum_wage"`
	AverageWage       float64   `json:"average_wage"`
	MedianWage        float64   `json:"median_wage"`
	MaximumWage       float64   `json:"maximum_wage"`
	WorkerWageDeciles []float64 `json:"worker_wage_deciles"`

	CapFirmsIncome 				float64 `json:"cap_firms_income"`
	ConFirmsIncome       	float64 `json:"con_firms_income"`
	FirmsIncome						float64	`json:"firms_income"`
	//AverageImitationRate float64 `json:"average_imitation_rate"`
	//AverageRadicalInnovationRate float64 `json:"average_radical_innovation_rate"`
	CreatedTechnologiesCount int `json:"created_technologies_count"`
	MinimumTechGeneration    int `json:"minimum_tech_generation"`
	AverageTechGeneration    int `json:"average_tech_generation"`
	MaximumTechGeneration    int `json:"maximum_tech_generation"`
	HighestTechGenerationEmerged int `json:"highest_tech_generation_emerged"`
	TechnologiesSold				 int `json:"tech_sold"`

	AggregatedConsumption float64 `json:"aggregated_consumption"`
}
