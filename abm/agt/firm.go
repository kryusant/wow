package agt

import (
	"fmt"
	"ia04_wow/utils"
	"log"
	"math"
	r "math/rand"
	"sort"
	"sync"
	"time"

	"gonum.org/v1/gonum/stat/distuv"
)

var countFirm = int(0)

const W_ERR = 0.1 // Standard deviation/error for wage estimation
const timeFactor = 10
const maxJobAcceptance = 1000

var timeOutOrdering = time.Second * 4 / timeFactor
var timeOutReceiveOrder = time.Second * 5 / timeFactor

// FIRMS STRUCT ###################################

type Firm struct {
	ID                 int
	OfferedWage        float64
	OfferedWagePremium float64
	PastSales          []float64
	MarketShare        float64 // <?> C'est quoi
	// CandidatesQueue     []Worker // TODO: change UML
	ApplicationsQueue     []WorkerJobRequest
	WorkForce             []*Worker
	ExpectedProduction    float64 // Q
	EffectiveProductivity []float64
	AverageSkillLevel     float64
	Savings               float64

	// JOB REQUEST COMMUNICATION
	jobRequestChan            chan WorkerJobRequest
	ReceivedJobAcceptanceChan chan WorkerJobAcceptanceResponse
	jobRequestCapacity        int
}

type ConsumptionGoodFirm struct {
	Firm
	ReceivedCatalogs        []Catalog
	CapitalStock            []Technology
	OrderedCatalogs         []Catalog
	CurrentDemand           float64
	Demand                  []float64
	DesiredProduction       []float64
	TechnologyReceivingChan chan Technology
}

type CapitalGoodFirm struct {
	Firm
	Technology                     Technology
	NumberOfTechnologies           int
	CurrentTechnologicalGeneration int
	InnovationWorkersNumber        int
	ImitationWorkersNumber         int
	RDWorkersNumber                int
	UsualWorkersNumber             int
	OrderChan                      chan Order
	Orders                         []Order
	CurrentDemand                  float64
	Demand                         []float64
}

func (f *Firm) GetID() int {
	return f.ID
}

func (f *Firm) GetJobSearchChannel() chan WorkerJobRequest {
	f.jobRequestChan = make(chan WorkerJobRequest)
	return f.jobRequestChan
}

func (f *Firm) GetMarketShare() float64 {
	return f.MarketShare
}

func (f *Firm) GetOfferedWage() float64 {
	return f.OfferedWage
}

func (f *Firm) WorksForCompany(workerID int) bool {
	for _, w := range f.WorkForce {
		if w.ID == workerID {
			return true
		}
	}
	return false
}

func (f *Firm) TypeWorker() bool {
	return false
}

func (f *Firm) JobSearchPhase(minWage float64) {
	// Wait for applications coming into the channel until time out
	//fmt.Printf("Applications are open for company %d.\n", f.ID)
	var timeOutApplications = time.Second * 10 / timeFactor
	var timeOutApplicationConfirmation = timeOutApplications + time.Second*30/timeFactor //40 / timeFactor
WaitingForApplications:
	for {
		select { // Traitement sur le tas ou attendre un certain temps, puis décider?
		case application := <-f.jobRequestChan:
			// Append candidates in the queue if requiredWage is lower than OfferedWage
			//fmt.Println("Application received by Company", f.ID, "for Worker", application.workerID, "asking for a wage of", application.desiredWage)
			if application.desiredWage <= f.OfferedWage { // Retain application
				f.ApplicationsQueue = append(f.ApplicationsQueue, application)

				// If bigger, refuse application answering the candidate
			} else { // Refuse application
				response := FirmJobRequest{
					firmID:         f.ID,
					offeredWage:    0,
					firmOfferRChan: nil,
				}
				application.workerOfferChan <- response
			}
		case <-time.After(timeOutApplications): // timeout application process.
			//close(f.jobRequestChan)
			break WaitingForApplications
			//default:
		}
	}

	if len(f.ApplicationsQueue) > 0 {
		f.SelectCandidates(minWage)
		for _, worst := range f.ApplicationsQueue[f.RequiredWorkforce():] { // Worst candidates
			response := FirmJobRequest{
				firmID:         f.ID,
				offeredWage:    0,
				firmOfferRChan: nil,
			}
			fmt.Println("Company", f.ID, "rejects Worker", worst.workerID, "❌")
			worst.workerOfferChan <- response
		}

		for _, best := range f.ApplicationsQueue[:f.RequiredWorkforce()] { // Best candidates
			response := FirmJobRequest{
				firmID:         f.GetID(),
				offeredWage:    f.OfferedWage,
				firmOfferRChan: f.ReceivedJobAcceptanceChan,
			}
			fmt.Println("Company", f.ID, "accepts Worker", best.workerID, "✔️")
			best.workerOfferChan <- response
		}

	WaitingForAcceptances:
		for {
			// Wait for best candidate's answer
			select {
			// Add to workforce those who answered yes
			case acceptance := <-f.ReceivedJobAcceptanceChan:
				if acceptance.quits == true {
					fmt.Println("🔥", f.ToStringShorter(), "firing worker", acceptance.worker.ToStringShorter())
					f.FireWorker(acceptance.worker)
				} else if acceptance.acceptance == true { // If a worker accepts
					for _, best := range f.ApplicationsQueue[:f.RequiredWorkforce()] { // First, verify if it was choosen for given application process
						if best.workerID == acceptance.worker.ID {
							f.AcceptWorker(acceptance.worker)
							break
						}
					}
				}
			case <-time.After(timeOutApplicationConfirmation):
				//close(f.ReceivedJobAcceptanceChan)
				break WaitingForAcceptances
				//default:
			}
		}
		// Erase candidates after the application phase has ended
		f.ApplicationsQueue = nil
	}
	fmt.Println("JobSearchPhase ended for", f.ToStringShorter())
}

func (f *Firm) FireWorker(worker *Worker) {
	//fmt.Println("Worker", worker.ToStringShorter(), "is retired from the workforce of Company", f.ID)
	idx := -1
	// Check if the index is within the bounds of the array
	for i, _ := range f.WorkForce {
		if worker == f.WorkForce[i] {
			idx = i
		}
	}

	if idx != -1 { //
		// Create a new slice with a capacity one element smaller than the original array
		newArr := make([]*Worker, len(f.WorkForce)-1)
		// fmt.Println("PREVIOUS", f.WorkForce)
		// Copy all elements from the original array except for the element at the specified index
		copy(newArr[:idx], f.WorkForce[:idx])
		copy(newArr[idx:], f.WorkForce[idx+1:])

		f.WorkForce = newArr
		// fmt.Println("NOW", f.WorkForce)

	}

}

func (f *Firm) AcceptWorker(worker *Worker) {
	//fmt.Println("Worker", worker.ToStringShorter(), "is added to the workforce of Company", f.ID)
	f.WorkForce = append(f.GetWorkForce(), worker)
}

func (f *Firm) Role() string {
	return "firm"

}

func (f *Firm) GetWorkForce() []*Worker {
	return f.WorkForce
}

func (f *Firm) ToStringShorter() string {
	return fmt.Sprintf("[F%d]", f.ID)
}

func (f *Firm) PrintWorkForce() {
	//fmt.Println(" -- Work force of company", f.ID)
	for _, w := range f.WorkForce {
		fmt.Print(f.ToStringShorter(), " 🧑‍💼 ", w.ID, "\n")
	}
}

func (f *Firm) PayWage() {
	//f.PrintWorkForce()
	for k, _ := range f.WorkForce {
		f.Savings -= f.OfferedWage
		f.WorkForce[k].Wage = f.OfferedWage
		//fmt.Println("PAYING the amount of", f.OfferedWage, "to", f.WorkForce[k].ToString())
	}
	//fmt.Println(f.ToStringShorter(), "has payed", f.OfferedWage, " to its employees.")
}

func (f *Firm) UpdateWagePremium() {
	// TODO: mettre vrai formule
	f.OfferedWagePremium = 1
}

func (f *Firm) UpdateOfferedWage(minWage float64) {
	f.OfferedWage = math.Max((1+f.OfferedWagePremium+(r.NormFloat64()*W_ERR))*f.OfferedWage, minWage)
}

func (f Firm) RequiredWorkforce() int {
	return 1 // TODO: Computation of the numbers of employees required
}

func (f *Firm) SortCandidates() {
	sort.Slice(f.ApplicationsQueue, func(i, j int) bool {
		return f.ApplicationsQueue[i].desiredWage < f.ApplicationsQueue[j].desiredWage
	})
}

func (f *Firm) SelectCandidates(minWage float64) {
	fmt.Println("Hired before at maximum wage of ", f.OfferedWage)
	f.UpdateWagePremium()
	f.UpdateOfferedWage(minWage)
	fmt.Println("Hiring now at maximum wage of ", f.OfferedWage)
	f.SortCandidates()
}

// Next 4 functions are not typed Firm because of Go constraints, but functions represents a base calculation of the firms, so they stay in this firm section.
func (conFirm *ConsumptionGoodFirm) UpdateEffectiveProductivity() {
	ap := 0.0 // in-firm aggregate productivity
	for _, worker := range conFirm.Firm.WorkForce {
		ap += conFirm.PotentialProductivityOfWorker(worker)
	}
	conFirm.Firm.EffectiveProductivity = append(conFirm.EffectiveProductivity, ap/float64(len(conFirm.Firm.WorkForce)))
}

func (conFirm *ConsumptionGoodFirm) PotentialProductivityOfWorker(w *Worker) float64 {
	// worker skills / average worker skills * tech productivity per worker
	sumProdOfTech := 0.0
	for _, tech := range conFirm.CapitalStock {
		sumProdOfTech += tech.LabourProductivityOfTechnology
	}
	meanProdOfTechPerWorker := sumProdOfTech / float64(len(conFirm.Firm.WorkForce))
	return w.Skills / conFirm.Firm.AverageSkillLevel * meanProdOfTechPerWorker
}

func (capFirm *CapitalGoodFirm) UpdateEffectiveProductivity() {
	ap := 0.0 // in-firm aggregate productivity
	fmt.Println("UpdateEffectiveProductivity of firm", capFirm.Firm.ID, "with", len(capFirm.Firm.WorkForce), "workers", "and", capFirm.UsualWorkersNumber, "usual workers")
	for _, worker := range capFirm.Firm.WorkForce[0:capFirm.UsualWorkersNumber] { // Do it only for the non R&D part of the workers
		ap += capFirm.PotentialProductivityOfWorker(worker)
	}
	capFirm.Firm.EffectiveProductivity = append(capFirm.EffectiveProductivity, ap/float64(len(capFirm.Firm.WorkForce)))
}

func (capFirm *CapitalGoodFirm) PotentialProductivityOfWorker(w *Worker) float64 {
	// worker skills / average worker skills * tech productivity per worker
	return w.Skills / capFirm.Firm.AverageSkillLevel * (capFirm.Technology.LabourProductivityOfTechnology * float64(capFirm.NumberOfTechnologies) / float64(len(capFirm.Firm.WorkForce)))
}

func (f *Firm) UpdateAverageSkillLevel() {
	averageSkillLevel := 0.0
	for _, worker := range f.WorkForce {
		averageSkillLevel += worker.Skills
	}
	f.AverageSkillLevel = averageSkillLevel / float64(len(f.WorkForce))
}

func (conFirm *ConsumptionGoodFirm) UpdateVariables() {
	conFirm.Firm.UpdateAverageSkillLevel()
	conFirm.UpdateEffectiveProductivity()
	conFirm.UpdateDesiredProduction()
	// conFirm.updateNeededCapitalStock
}

func (capFirm *CapitalGoodFirm) UpdateVariables(shareOfRDExpenditureInImitation float64, RDInvestmentPropensityOverSales float64) {
	capFirm.Firm.UpdateAverageSkillLevel()
	capFirm.SplitWorkForce(shareOfRDExpenditureInImitation, RDInvestmentPropensityOverSales)
	capFirm.UpdateEffectiveProductivity()
}

// CONSUMPTION-GOOD FIRM ##############################################

// NewConsumptionGoodFirm(100,100,10,[1000],1000,1000,[]WorkerJobRequest{},[]*Worker{},nil,nil)
func NewConsumptionGoodFirm(ow, owp float64, ps []float64, ms, ep float64) *ConsumptionGoodFirm {
	countFirm++
	return &ConsumptionGoodFirm{
		Firm: Firm{
			ID:                 countFirm,
			OfferedWage:        ow,
			OfferedWagePremium: owp,
			PastSales:          ps,
			MarketShare:        ms,
			ApplicationsQueue:  []WorkerJobRequest{},
			WorkForce:          []*Worker{},
			jobRequestChan:     nil,
			// Create channel for job acceptance
			ReceivedJobAcceptanceChan: make(chan WorkerJobAcceptanceResponse, maxJobAcceptance),
			ExpectedProduction:        ep,
		},
	}
}

func (conFirm *ConsumptionGoodFirm) UpdateDesiredProduction() {
	// Desired Production and Demand are the same for now in this model but should evolve
	if len(conFirm.Demand) > 0 { // <!> @OMAR: URGENT: Solution temporaire, il faut conFirm.Demand pour @Xingyu; tu as fini?
		// Vérifier les autres if que j'ai mis car c'est des solutions temporaires.
		conFirm.DesiredProduction = append(conFirm.DesiredProduction, conFirm.Demand[len(conFirm.Demand)-1])
	}
}

// DesiredCapitalStockInProductivity: derived equation from Firm Effective Productivity
func (conFirm *ConsumptionGoodFirm) DesiredCapitalStockInProductivity() float64 {

	sumPropProdWithoutTech := 0.0 // sum of proportional productivity of Workers without Technologies
	for _, w := range conFirm.Firm.WorkForce {
		sumPropProdWithoutTech += w.Skills / (conFirm.AverageSkillLevel * float64(len(conFirm.Firm.WorkForce)))
	}
	fmt.Println(sumPropProdWithoutTech)
	// it is the sum of the technologies' productivity needed in addition to the current set of technologies productivities to accomplish the desired production output
	desiredCapStock := (conFirm.DesiredProduction[len(conFirm.DesiredProduction)-1] - conFirm.Firm.EffectiveProductivity[len(conFirm.Firm.EffectiveProductivity)-1]) * float64(len(conFirm.Firm.WorkForce)) / sumPropProdWithoutTech
	//fmt.Println("Consumption firm", conFirm.Firm.ID, "desire capital stock of productivity", desiredCapStock)
	return desiredCapStock
}

// sortTechnologiesInCatalogByProductivity: sort Technologies in descending order
func (conFirm *ConsumptionGoodFirm) sortTechnologiesInCatalogByProductivity() {
	sort.Slice(conFirm.ReceivedCatalogs, func(i, j int) bool {
		return conFirm.ReceivedCatalogs[i].Technology.LabourProductivityOfTechnology > conFirm.ReceivedCatalogs[j].Technology.LabourProductivityOfTechnology
	})
}

func (conFirm *ConsumptionGoodFirm) DesiredCapitalStockInPrice(depreciationRateOfCapital float64) float64 {
	avgPriceCatalogTechs := 0.0 // AveragePriceOfCatalogTechnologies
	for _, catalog := range conFirm.ReceivedCatalogs {
		avgPriceCatalogTechs += catalog.Technology.Price
	}
	avgPriceCatalogTechs = avgPriceCatalogTechs / float64(len(conFirm.ReceivedCatalogs))
	rentalPriceOfCapital := depreciationRateOfCapital * avgPriceCatalogTechs
	return 0.5 * conFirm.PastSales[len(conFirm.PastSales)-1] / rentalPriceOfCapital * conFirm.DesiredProduction[len(conFirm.DesiredProduction)-1]
}

func (conFirm *ConsumptionGoodFirm) ReceiveCatalogs(setOfCatalogs []Catalog) {
	conFirm.ReceivedCatalogs = setOfCatalogs
}

func (conFirm *ConsumptionGoodFirm) ChooseTechnologyToOrder(depreciationRateOfCapital float64) {
	desiredCapStockInPrice := conFirm.DesiredCapitalStockInPrice(depreciationRateOfCapital) // nil
	desiredCapStockInProd := conFirm.DesiredCapitalStockInProductivity()                    // nil
	conFirm.sortTechnologiesInCatalogByProductivity()

	conFirm.OrderedCatalogs = nil

	for i := 0; i < len(conFirm.ReceivedCatalogs) && desiredCapStockInProd > 0 && desiredCapStockInPrice > -0.5*conFirm.Firm.PastSales[len(conFirm.Firm.PastSales)-1]; i += 1 {

		if conFirm.ReceivedCatalogs[i].Technology.LabourProductivityOfTechnology > desiredCapStockInProd {
			// if the most productive tech in catalog is too productive compared to what's needed
			i += 1
		} else {
			fmt.Println("Firm", conFirm.Firm.ID, "wants to order technology from firm", conFirm.ReceivedCatalogs[i].Technology.CapFirmID)
			conFirm.OrderedCatalogs = append(conFirm.OrderedCatalogs, conFirm.ReceivedCatalogs[i])
			desiredCapStockInProd -= conFirm.ReceivedCatalogs[i].Technology.LabourProductivityOfTechnology
			desiredCapStockInPrice -= conFirm.ReceivedCatalogs[i].Technology.Price
		}
	}
	// kryusant:
	// TODO Delete Old Technologies
	// TODO Integrate investment in simulation with parameters (set param for investment possibilities instead of the 0.5 in ChooseTechnologyToOrder())
	// TODO Debug DesiredCapitalStockInProductivity() that causes trouble for delivering technologies
}

func (conFirm *ConsumptionGoodFirm) OrderTechnologies() {
	conFirm.TechnologyReceivingChan = make(chan Technology)
	order := Order{
		firmID:        conFirm.Firm.ID,
		firmOrderChan: conFirm.TechnologyReceivingChan,
	}
	// no need of wait groups as orders can be refused
	// because of stocks problem in producers' side
	// var wg sync.WaitGroup
	for _, catalog := range conFirm.OrderedCatalogs {
		// wg.Add(1)
		go func(InChannel chan Order) {
			// defer wg.Done()
			InChannel <- order
		}(catalog.FirmProdChan)
	}
	// wg.Wait()
}

func (conFirm *ConsumptionGoodFirm) ReceiveDeliveries(c chan int) {
	if conFirm.OrderedCatalogs != nil {
		for i := 0; i < len(conFirm.OrderedCatalogs); i += 1 {
			go func(InChannel chan Technology) {
				select { // Traitement sur le tas ou attendre un certain temps,
				case tech := <-InChannel:
					// Append received technology in Capital Stock of the consumption-good firm
					fmt.Println("Technology delivered by Company", tech.CapFirmID, "for Company", conFirm.Firm.ID, "giving a technology of price ", tech.Price)
					conFirm.CapitalStock = append(conFirm.CapitalStock, *CopyTechnology(tech))
					fmt.Println("ok")
				case <-time.After(timeOutReceiveOrder): // timeout ordering process.
					//close(order.firmOrderChan)
					fmt.Println("Firm", conFirm.Firm.ID, "didn't receive its order.")
					break
					//default:
				}
			}(conFirm.TechnologyReceivingChan)
		}
	}
	c <- 1

}

func (conFirm *ConsumptionGoodFirm) IncreaseDemand(consumption float64) {
	if consumption < 0 {
		panic("bro")
	}
	conFirm.CurrentDemand += consumption
}

// CAPITAL-GOOD FIRM ##################################################

// NewCapitalGoodFirm(100,100,10,[1000],1000,1000,[]WorkerJobRequest{},[]*Worker{},nil,nil)
func NewCapitalGoodFirm(ow, owp float64, ps []float64, ep, ms float64, lptech, lpnbtech float64) *CapitalGoodFirm {
	countFirm++
	return &CapitalGoodFirm{
		Firm: Firm{
			ID:                        countFirm,
			OfferedWage:               ow,
			OfferedWagePremium:        owp,
			PastSales:                 ps,
			MarketShare:               ms,
			ApplicationsQueue:         []WorkerJobRequest{},
			WorkForce:                 []*Worker{},
			jobRequestChan:            nil,
			ReceivedJobAcceptanceChan: make(chan WorkerJobAcceptanceResponse, maxJobAcceptance),
			ExpectedProduction:        ep,
		},
		Technology: *AddTechnology(
			countFirm,
			false,
			lptech,
			lpnbtech,
			0.0,
			0.0,
			0,
		),
		NumberOfTechnologies:           1,
		CurrentTechnologicalGeneration: 1,
	}
}

func (capFirm *CapitalGoodFirm) SplitWorkForce(shareOfRDExpenditureInImitation float64, RDInvestmentPropensityOverSales float64) {
	fmt.Println("Firm", capFirm.Firm.ID, "split its work force according to past sales = ", capFirm.PastSales[len(capFirm.PastSales)-1], "and R&D workers number:", int(math.Round(RDInvestmentPropensityOverSales*capFirm.PastSales[len(capFirm.PastSales)-1]/capFirm.GetOfferedWage())))
	capFirm.RDWorkersNumber = int(math.Round(RDInvestmentPropensityOverSales * capFirm.PastSales[len(capFirm.PastSales)-1] / capFirm.GetOfferedWage()))
	capFirm.UsualWorkersNumber = len(capFirm.GetWorkForce()) - capFirm.RDWorkersNumber
	capFirm.ImitationWorkersNumber = int(shareOfRDExpenditureInImitation * float64(capFirm.RDWorkersNumber))
	capFirm.InnovationWorkersNumber = capFirm.RDWorkersNumber - capFirm.ImitationWorkersNumber
}

func (capFirm *CapitalGoodFirm) NormalizedShareOfInnovationWorkers() float64 {
	return float64(capFirm.InnovationWorkersNumber) / float64(capFirm.RDWorkersNumber)
}

func (capFirm *CapitalGoodFirm) NormalizedShareOfImitationWorkers() float64 {
	return float64(capFirm.ImitationWorkersNumber) / float64(capFirm.RDWorkersNumber)
}

func (capFirm *CapitalGoodFirm) AccessToRadicalInnovation(accessToNewGeneration float64, normalizedShareOfInnovativeRDWorkers float64) bool {
	prob := 1 - math.Exp(-accessToNewGeneration*normalizedShareOfInnovativeRDWorkers)
	r.Seed(time.Now().UnixNano())
	b := distuv.Bernoulli{P: prob}
	return utils.Ftob(b.Rand())
}

func (capFirm *CapitalGoodFirm) AccessToIncrementalInnovation(searchCapabilityForInnovation float64, normalizedShareOfInnovativeRDWorkers float64) bool {
	prob := 1 - math.Exp(-searchCapabilityForInnovation*normalizedShareOfInnovativeRDWorkers)
	r.Seed(time.Now().UnixNano())
	b := distuv.Bernoulli{P: prob}
	return utils.Ftob(b.Rand())
}

func (capFirm *CapitalGoodFirm) Innovate(accessToIncrementalInnovation bool, accessToRadicalInnovation bool, highestGenerationOnMarket int, alpha float64, beta float64, betaDistSupport []float64, effectivenessOfOpportunitiesExploitation float64) {
	if accessToIncrementalInnovation {
		supportNormalizationTranslation := (betaDistSupport[0]+betaDistSupport[1])/2 - 0.5 // 0.5 is the mean of the usual support of the beta distribution [0,1]
		supportNormalizationShrinking := betaDistSupport[1] - betaDistSupport[0]
		r.Seed(time.Now().UnixNano())
		b := distuv.Beta{Alpha: alpha, Beta: beta}
		newLabourProductivityOfTechnology := capFirm.Technology.LabourProductivityOfTechnology * (1 + b.Rand()*supportNormalizationShrinking + supportNormalizationTranslation)
		r.Seed(time.Now().UnixNano())
		b = distuv.Beta{Alpha: alpha, Beta: beta}
		labourProductivityNeededToBuildTechnology := capFirm.Technology.LabourProductivityNeededToBuildTechnology * (1 + b.Rand()*supportNormalizationShrinking + supportNormalizationTranslation)

		capFirm.Technology.LabourProductivityOfTechnology = newLabourProductivityOfTechnology
		capFirm.Technology.LabourProductivityNeededToBuildTechnology = labourProductivityNeededToBuildTechnology
		capFirm.Technology.Disruptive = false
	}
	if accessToRadicalInnovation && capFirm.CurrentTechnologicalGeneration < highestGenerationOnMarket {
		minA := capFirm.Technology.LabourProductivityOfTechnology
		maxA := (1 + effectivenessOfOpportunitiesExploitation) * capFirm.Technology.LabourProductivityOfTechnology
		r.Seed(time.Now().UnixNano())
		u := distuv.Uniform{Min: minA, Max: maxA}
		newLabourProductivityOfTechnology := u.Rand()
		lastLabourProductivityOfTechnology := capFirm.Technology.LabourProductivityOfTechnology
		capFirm.Technology.LabourProductivityOfTechnology = newLabourProductivityOfTechnology

		minB := (lastLabourProductivityOfTechnology * capFirm.Technology.LabourProductivityNeededToBuildTechnology) / newLabourProductivityOfTechnology
		maxB := (1 + effectivenessOfOpportunitiesExploitation) * ((lastLabourProductivityOfTechnology * capFirm.Technology.LabourProductivityNeededToBuildTechnology) / newLabourProductivityOfTechnology)
		r.Seed(time.Now().UnixNano())
		u = distuv.Uniform{Min: minB, Max: maxB}
		capFirm.Technology.LabourProductivityNeededToBuildTechnology = u.Rand()

		capFirm.CurrentTechnologicalGeneration += 1
		capFirm.Technology.Generation += 1
		capFirm.Technology.Disruptive = true
	}
}

func (capFirm *CapitalGoodFirm) AccessToImitation(searchCapabilityForImitation float64, normalizedShareOfInnovativeRDWorkers float64) bool {
	prob := 1 - math.Exp(-searchCapabilityForImitation*normalizedShareOfInnovativeRDWorkers)
	r.Seed(time.Now().UnixNano())
	b := distuv.Bernoulli{P: prob}
	return utils.Ftob(b.Rand())
}

func (capFirm *CapitalGoodFirm) Imitate(accessToImitation bool, TechnologyOfCompetitorToImitate Technology) {
	if accessToImitation {
		// copy technology
		tech := TechnologyOfCompetitorToImitate
		capFirm.Technology = tech
		capFirm.Technology.Disruptive = false
	}
}

// ProduceTechnologies: calculate the new Number Of Technologies that will be produced and their properties
func (capFirm *CapitalGoodFirm) ProduceTechnologies(markUpInCapitalGoodSector float64) {
	fmt.Println("Firm", capFirm.Firm.ID, "update its number of technologies by producing them.")
	if capFirm.Firm.EffectiveProductivity == nil {
		// TODO initialize Technology:
		capFirm.NumberOfTechnologies = 1
	} else {
		capFirm.NumberOfTechnologies = int(capFirm.Technology.LabourProductivityNeededToBuildTechnology / capFirm.Firm.EffectiveProductivity[len(capFirm.EffectiveProductivity)-1])
		fmt.Println(capFirm.Firm.EffectiveProductivity)
	}
	capFirm.Technology.ProductionCost = capFirm.GetOfferedWage() / capFirm.Technology.LabourProductivityNeededToBuildTechnology
	// OfferedWage attribute is equal to the monetary average
	// wage, because the wage is homogeneous in one firm.
	capFirm.Technology.CapFirmID = capFirm.Firm.ID
	capFirm.Technology.Price = (1 + markUpInCapitalGoodSector) * capFirm.Technology.ProductionCost
	fmt.Println("Firm", capFirm.Firm.ID, "produced", capFirm.NumberOfTechnologies, "units of technology at individual price of", capFirm.Technology.Price, "being the Labour Needed to Produce Technology B =", capFirm.Technology.LabourProductivityNeededToBuildTechnology)
}

func (capFirm *CapitalGoodFirm) ReceiveOrders(c chan int) {
	capFirm.Orders = nil
	capFirm.OrderChan = make(chan Order)
	fmt.Println("Firm", capFirm.Firm.ID, "waits for orders.")
WaitingForOrders:
	for i := 0; i < capFirm.NumberOfTechnologies; i += 1 {
		capFirm.CurrentDemand = 0
		select {
		case order := <-capFirm.OrderChan:
			capFirm.Orders = append(capFirm.Orders, order)
			capFirm.CurrentDemand += capFirm.Technology.Price
		case <-time.After(timeOutOrdering):
			log.Println("Firm", capFirm.Firm.ID, "stopped waiting for orders because it's too late.")
			break WaitingForOrders
		}
	}
	capFirm.Demand = append(capFirm.Demand, capFirm.CurrentDemand)
	log.Println("Consumption good firm", capFirm.Firm.ID, "'s current demand (and thus income) is", capFirm.CurrentDemand)

	close(capFirm.OrderChan)
	c <- 1
}

func (capFirm *CapitalGoodFirm) DeliverTechnologies() {
	//	Send the request to each firm and wait for an answer we'll put in a slice/array
	for i := 0; i < capFirm.NumberOfTechnologies; i += 1 {
		var wg sync.WaitGroup
		for _, order := range capFirm.Orders {
			wg.Add(1)
			go func(InChannel chan Technology) {
				defer wg.Done()
				InChannel <- capFirm.Technology
			}(order.firmOrderChan)
		}
		wg.Wait()
	}
}

func (capFirm *CapitalGoodFirm) labourDemand(orders int, labourProductivity float64) int {
	return 0 // TODO, also for consumption-good firms that is now named RequiredWorkforce()
}
