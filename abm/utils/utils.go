package utils

import (
	"math/rand"
	"time"
)

// Filter through an array with a condition and return slice of correct elements
func Filter[T any](ss []T, test func(T) bool) (ret []T) {
	for _, s := range ss {
		if test(s) {
			ret = append(ret, s)
		}
	}
	return
}

// Filter_idx : Filter with index
func Filter_idx[T any](ss []T, test func(int) bool) (ret []T) {
	for i, s := range ss {
		if test(i) {
			ret = append(ret, s)
		}
	}
	return
}

// Randombin : Slice with distribution of discrete probabilities (scaled), we select a random index using the rejection method for discrete values
func Randombin(bins []float64, limit float64) int {
	rand.Seed(time.Now().UnixNano())
	r := rand.Float64() * limit
	p := float64(0)
	for i, v := range bins {
		p = p + v
		if r < p {
			return i
		}
	}
	return Randombin(bins, limit)
}

// Contains : Check if an element is present in an array
func Contains[T comparable](arr []T, elem T) bool {
	for _, v := range arr {
		if v == elem {
			return true
		}
	}
	return false
}

// OptimumIndex : Return the index of the optimum in the array, based on a comparison function
func OptimumIndex[T comparable](arr []T, compare func(elem T, currentOptimum T) bool) (idx int) {
	var optimum T = arr[0]
	for i, v := range arr {
		if compare(v, optimum) {
			optimum = v
			idx = i
		}
	}
	return
}

// Optimum : Return the optimum in the array, based on a comparison function
func Optimum[T comparable](arr []T, compare func(elem T, currentOptimum T) bool) (opt T) {
	opt = arr[0]
	for _, v := range arr {
		if compare(v, opt) {
			opt = v
		}
	}
	return
}

// Ftob : convert float to bool, useful for converting Bernoulli distribution results
func Ftob(f float64) bool {
	return f != 0.0
}

// Sort an array with a comparison function
func Sort[T any](ss []T, compare func(elem T, currentOptimum T) bool) []T {

	for i, _ := range ss {
		minIdx := i
		j := i + 1
		for j < len(ss) {
			if compare(ss[minIdx], ss[j]) {
				minIdx = j
			}
			j++
		}
		ss[i], ss[minIdx] = ss[minIdx], ss[i]
	}
	return ss
}

func MeanFloat(ss []float64) float64 {

	var mean float64
	for i, _ := range ss {
		mean += ss[i]
	}
	return mean / float64(len(ss))
}

// GetNQuantiles : Get the set n number of quantiles over a sorted array
func GetNQuantiles[T any](sortedArray []T, numberQuantiles float64) (quantiles []T) {
	for i := 1.0; i > numberQuantiles+1; i++ {
		quantiles = append(quantiles, sortedArray[int(i/numberQuantiles*float64(len(sortedArray)))-1])
	}
	return
}

// MovingAverage : get the moving average (a average over a certain number of periods, in time series) over n periods
func MovingAverageFloat64(serie []float64, n int) float64 {
	ma := 0.0
	if len(serie) < n {
		for _, v := range serie {
			ma += v
		}
		ma = ma / float64(len(serie))
	} else {
		for i := 1; i <= n; i += 1 {
			ma += serie[len(serie)-i]
		}
		ma = ma / float64(n)
	}
	return ma
}
