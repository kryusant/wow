package utils

import (
	"reflect"
	"testing"
)

func TestFilter(t *testing.T) {
	arr := []int{1, 2, 3, 4, 5, 6}
	even := func(x int) bool {
		return x%2 == 0
	}

	res := Filter(arr, even)

	if res[0] != 2 {
		t.Errorf("error, result for 0 should be 2, %d computed", res[0])
	}
	if res[1] != 4 {
		t.Errorf("error, result for 1 should be 4, %d computed", res[1])
	}
	if res[2] != 6 {
		t.Errorf("error, result for 2 should be 6, %d computed", res[2])
	}
}

func TestFilter_idx(t *testing.T) {
	arr := []int{1, 2, 3, 4, 5, 6}
	even := func(x int) bool {
		return x%2 == 0
	}

	res := Filter_idx(arr, even)

	if res[0] != 1 {
		t.Errorf("error, result for 0 should be 1, %d computed", res[0])
	}
	if res[1] != 3 {
		t.Errorf("error, result for 1 should be 3, %d computed", res[1])
	}
	if res[2] != 5 {
		t.Errorf("error, result for 2 should be 5, %d computed", res[2])
	}
}

func TestRandombin(t *testing.T) {
	type args struct {
		bins  []float64
		limit float64
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{name: ".0 | 1.0", args: args{bins: []float64{.0, 1.0}, limit: 1}, want: 1},
		{name: "1.0 | .0", args: args{bins: []float64{1.0, .0}, limit: 1}, want: 0},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Randombin(tt.args.bins, tt.args.limit); got != tt.want {
				t.Errorf("Randombin() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestContains(t *testing.T) {
	type T int
	type args struct {
		arr  []T
		elem T
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{name: "int present", args: args{arr: []T{1, 45, 69, 12, 5}, elem: 12}, want: true},
		{name: "int not present", args: args{arr: []T{1, 45, 69, 12, 5}, elem: 10}, want: false},
	}

	//tests = append(tests, struct {
	//	name string
	//	args args
	//	want bool
	//}{name: "bools present", args: args{arr: []T{true, true, false}, elem: false}, want: true})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Contains(tt.args.arr, tt.args.elem); got != tt.want {
				t.Errorf("Contains() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestOptimumIndex(t *testing.T) {
	type T int
	type args struct {
		arr     []T
		compare func(elem T, currentOptimum T) bool
	}
	tests := []struct {
		name    string
		args    args
		wantIdx int
	}{
		{name: "max", args: args{arr: []T{45, 69, 17, 1, 78, 63, 2}, compare: func(elem T, currentOptimum T) bool { return elem > currentOptimum }}, wantIdx: 4},
		{name: "min", args: args{arr: []T{45, 69, 17, 1, 78, 63, 2}, compare: func(elem T, currentOptimum T) bool { return elem < currentOptimum }}, wantIdx: 3},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotIdx := OptimumIndex(tt.args.arr, tt.args.compare); gotIdx != tt.wantIdx {
				t.Errorf("OptimumIndex() = %v, want %v", gotIdx, tt.wantIdx)
			}
		})
	}
}

func TestFtob(t *testing.T) {
	type args struct {
		f float64
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{name: "0.0", args: args{0.0}, want: false},
		{name: "0.452", args: args{0.452}, want: true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Ftob(tt.args.f); got != tt.want {
				t.Errorf("Ftob() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestSort(t *testing.T) {
	type args struct {
		ss      []float64
		compare func(elem float64, currentOptimum float64) bool
	}
	tests := []struct {
		name string
		args args
		want []float64
	}{
		{name: "0.0", args: args{[]float64{2.0, 7.0, 4.0, 6.0, 3.0, 5.0}, func(wage1, wage2 float64) bool { return wage1 > wage2 }}, want: []float64{2.0, 3.0, 4.0, 5.0, 6.0, 7.0}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Sort(tt.args.ss, tt.args.compare); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Sort() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMeanFloat(t *testing.T) {
	type args struct {
		ss []float64
	}
	tests := []struct {
		name string
		args args
		want float64
	}{
		{name: "0.0", args: args{[]float64{2.0, 7.0, 4.0, 6.0, 3.0, 5.0}}, want: 4.5},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := MeanFloat(tt.args.ss); got != tt.want {
				t.Errorf("MeanFloat() = %v, want %v", got, tt.want)
			}
		})
	}
}
