package wow

import (
	"fmt"
	"ia04_wow/agt"
	"ia04_wow/utils"
	"log"
	"math"
	"math/rand"
	"sync"
	"time"

	"gonum.org/v1/gonum/stat/distuv"
)

type WorkOnWork struct {
	sync.RWMutex

	Active bool
	// An AgentI-Based Model is run for a set number of epochs, we may also need to get the current epoch at any moment
	epochs       int
	currentEpoch int

	//********- AGENTS -********/
	agents   []AgentI
	workers  []*agt.Worker
	conFirms []*agt.ConsumptionGoodFirm
	capFirms []*agt.CapitalGoodFirm
	gov      *agt.Government

	//********- REPORTING -********/
	// This function is made and set by the user as a way to retrieve wanted metrics about the ABM world, it will be called at each epoch
	reportingChannel         chan agt.WorldState
	highestGenerationEmerged int
	Report                   agt.WorldState

	//********- PARAMETERS -********/

	Param Param

	//********- Job Search -********/
	// Number of jobs each worker applies to at each epoch
	jbRandJobs int

	//********- AGGREGATE VARIABLES -********/

	//all slices to keep track of history
	aggregateProductivity   []float64
	aggregateWages          []float64
	averageWage             []float64
	conFirmsAggregateIncome []float64
	capFirmsAggregateIncome []float64
	firmsAggregateIncome    []float64
	technologiesSold        int
}

func (wow *WorkOnWork) PrintPtrs() {
	fmt.Println("===========>>>>>>>>>>>PRINT PTRS =================")
	for _, a := range wow.agents {
		fmt.Printf("%p  ", a)
	}
	fmt.Println("==== capfirm")
	for _, a := range wow.capFirms {
		fmt.Printf("company %p", a)
		fmt.Println(" ---- workforce ")
		for _, w := range a.WorkForce {
			fmt.Printf("worker %p", w)
		}
	}

	fmt.Println("===== confirm")
	for _, a := range wow.conFirms {
		fmt.Printf("%p", a)
		fmt.Println(" ---- workforce ")
		for _, w := range a.WorkForce {
			fmt.Printf("%p", w)
		}
	}
}

// NewABMSkeleton : Constructor (all default values are nil, or 0 for integers)
func NewABMSkeleton() *WorkOnWork {
	return &WorkOnWork{jbRandJobs: 2, highestGenerationEmerged: 1, Param: getParameters(), Report: agt.WorldState{Epoch: -1}}
}

// ResetABMSkeleton : Constructor (all default values are nil, or 0 for integers)
func (wow *WorkOnWork) ResetABMSkeleton() {
	*wow = WorkOnWork{jbRandJobs: 2, highestGenerationEmerged: 1, Param: getParameters(), Report: agt.WorldState{Epoch: -1}}
	fmt.Println(wow)
}

// SetEpochs : Set the number of epochs for which the ABM is going to run (only once)
func (wow *WorkOnWork) SetEpochs(epochs int) {
	wow.Lock()
	defer wow.Unlock()
	if wow.epochs == 0 {
		wow.epochs = epochs
	}
}

// Epochs : We may need to retrieve the set epochs
func (wow *WorkOnWork) Epochs() int {
	wow.RLock()
	defer wow.RUnlock()
	return wow.epochs
}

// CurrentEpoch : Return the current epoch of the simulation
func (wow *WorkOnWork) CurrentEpoch() int {
	wow.RLock()
	defer wow.RUnlock()
	return wow.currentEpoch
}

// AddAgent : Add an AgentI to the ABM
func (wow *WorkOnWork) AddAgent(agent AgentI) {
	// wow.Lock()
	// defer wow.Unlock()
	wow.agents = append(wow.agents, agent)
}

// AddGovernment : Add the government to the ABM
func (wow *WorkOnWork) AddGovernment(g *agt.Government) {
	// wow.Lock()
	// defer wow.Unlock()
	wow.gov = g
	//wow.AddAgent(g) (TODO)
}

// AddWorker : Add a Worker to the ABM
func (wow *WorkOnWork) AddWorker(worker *agt.Worker) {
	// wow.Lock()
	// defer wow.Unlock()
	wow.workers = append(wow.workers, worker)
	wow.AddAgent(worker)
}

// AddConFirm / AddCapFirm : Add a Firm to the ABM
func (wow *WorkOnWork) AddConFirm(firm *agt.ConsumptionGoodFirm) {
	wow.Lock()
	defer wow.Unlock()
	wow.conFirms = append(wow.conFirms, firm)
	wow.AddAgent(firm)
}

func (wow *WorkOnWork) AddCapFirm(firm *agt.CapitalGoodFirm) {
	wow.Lock()
	defer wow.Unlock()
	wow.capFirms = append(wow.capFirms, firm)
	wow.AddAgent(firm)
}

// TotalAgents = Return the total number of Agents
func (wow *WorkOnWork) TotalAgents() int {
	wow.RLock()
	defer wow.RUnlock()
	return len(wow.agents)
}

// TotalAgentsWithCriteria : Return the total number of Agents of a particular criteria, using a discriminator function (like the Role for example)
func (wow *WorkOnWork) TotalAgentsWithCriteria(discriminator func(agent AgentI) bool) int {
	wow.RLock()
	defer wow.RUnlock()
	total := 0
	for _, agent := range wow.agents {
		if discriminator(agent) {
			total++
		}
	}
	return total
}

// SetReportingChannel : Set the function used to retrieve information at every epoch
func (wow *WorkOnWork) SetReportingChannel(reportingChan chan agt.WorldState) {
	wow.Lock()
	defer wow.Unlock()
	wow.reportingChannel = reportingChan
}

func (wow *WorkOnWork) setCurrentEpoch(cEpoch int) {
	wow.Lock()
	defer wow.Unlock()
	wow.currentEpoch = cEpoch
}

func (wow *WorkOnWork) printAgents() {
	fmt.Println(" ---------------------- ")
	for _, worker := range wow.workers {
		fmt.Println(worker.ToString())
	}
	fmt.Println(" ---------------------- ")
}

// Simulate : The most important method, start the overall simulation and all agents in a wait group every epoch, calling the reportingChannel at every iteration
func (wow *WorkOnWork) Simulate() {
	// A set-up phase before running the timeline of events, classic job search but with minimum affectation
	wow.InitialJobAffectation()
	wow.InitializeFirmVariables()
	for i := 0; i < wow.epochs && wow.Active == true; i++ {
		//wow.PrintPtrs()
		fmt.Println("[⌛] --- ⌛ CURRENT EPOCH", i, " ⌛ --- ")
		wow.setCurrentEpoch(i)

		// # 3. Sets of technologies ordered in the previous period (if
		// any) are delivered
		wow.DeliveringSimulate()

		// # 4. Capital-good firm perform R&D
		// (incremental and radical innovation, and imitation)
		// and send signal the new machines to the consumption-good firms
		wow.InnovationProcessSimulate()
		wow.SignalMachinesSimulate()

		// #7. Machines from capital-good sector are ordered
		// for next period by consumption-good firm
		wow.OrderingSimulate()

		// #8. and #9. Job-seeking workers send a set of job
		// applications (apply()). Firms hire candidates (selectCandidates())
		// and worker retire and wages are set (updateOfferedWage(),
		// offeredWagePremium(), and skills adapted adaptSkillsToFirm())
		wow.JobSearchSimulate()

		// #10. Firms pay wages/bonuses and government pays
		// unemployment benefits (and firms compute average wage)
		// <#> Workers get payed
		wow.PaymentPhase()

		// #11. Consumption of workers occurs
		wow.ConsumptionSimulate()

		// #14. Aggregated variables are computed and last agents
		// updates their variables
		wow.UpdateGovernmentVariables()
		wow.UpdateUnemploymentBenefitForWorkers()
		wow.UpdateFirmVariables()
		wow.UpdateAggregateVariables()

		// We call the user-made function to get all the data we want for the current epoch
		// wow.reportingChannel <- wow.ReportWorldState()
		wow.Report = wow.ReportWorldState()
		fmt.Println(wow.ReportWorldState())
	}
	wow.Active = false
}

func (wow *WorkOnWork) UpdateGovernmentVariables() {
	wow.UpdateAggregateProductivity()
	wow.gov.Update(
		wow.Param.FractionOfAverageWageForUnemploymentBenefit,
		wow.Param.AggregateProductivityPassThrough,
		wow.averageWage,
		wow.aggregateProductivity,
	)
}

func (wow *WorkOnWork) UpdateFirmVariables() {
	for _, conFirm := range wow.conFirms {
		conFirm.UpdateVariables()
	}
	for _, capFirm := range wow.capFirms {
		capFirm.UpdateVariables(wow.Param.ShareOfRDExpenditureInImitation, wow.Param.RDInvestmentPropensityOverSales)
	}
}

func (wow *WorkOnWork) UpdateUnemploymentBenefitForWorkers() {
	uB := wow.gov.GetUnemploymentBenefit()
	for k, _ := range wow.workers {
		wow.workers[k].SetUnemploymentBenefit(uB)
	}
}

// InitialJobAffectation : We set an unemployment rate in mind, affect each worker to a firm, for now in equivalent proportions (based on market share better ?)
func (wow *WorkOnWork) InitialJobAffectation() {
	wow.Lock()
	defer wow.Unlock()
	fmt.Println(" --- Initial Job Affectation --- ")
	// We select a number of workers corresponding to the total count of firms and run a job search phase
	workerIdx := 0
	i := 0
	// increment workerIdx until it reaches the cutoff for the employment ratio
	cutoff := int(wow.Param.InitialEmploymentRatio * float64(len(wow.workers)))
	for workerIdx <= cutoff {
		//	Go through the next n firms (n = total count of firms)
		if i >= len(wow.capFirms)+len(wow.conFirms) {
			i = 0
		}
		// in order, capital good firms then consumer good firms
		if i < len(wow.capFirms) {
			// Affect worker to firm
			wow.capFirms[i].AcceptWorker(wow.workers[workerIdx])
			// Give job to worker
			wow.workers[workerIdx].JobFound(wow.capFirms[i].ID, wow.Param.InitialMinimumWage, wow.capFirms[i].ReceivedJobAcceptanceChan)
		} else {
			// Affect worker to firm
			wow.conFirms[i-len(wow.capFirms)].AcceptWorker(wow.workers[workerIdx])
			// Give job to worker
			wow.workers[workerIdx].JobFound(wow.conFirms[i-len(wow.capFirms)].ID, wow.Param.InitialMinimumWage, wow.conFirms[i-len(wow.capFirms)].ReceivedJobAcceptanceChan)
		}
		workerIdx++
		i++
	}
	fmt.Println(" ---- End of Initial Job Affectation  ---")
}

func (wow *WorkOnWork) JobSearchSimulate() {
	wow.SetJobSearchChannels()
	var wg sync.WaitGroup
	fmt.Println("[🔍] -------- 🔍 LAUNCHING JOB SEARCH FOR ALL AGENTS 🔍 -------- ")
	for i := 0; i < wow.TotalAgents(); i++ {
		wg.Add(1)
		go func(idx int) {
			defer wg.Done()
			wow.agents[idx].JobSearchPhase(wow.gov.GetMinimumWage())
		}(i)
	}
	// The epoch ends when all agents have finished running
	wg.Wait()
	fmt.Println("[🔍] -------- 🔍 JOB SEARCH PHASE END 🔍 -------- ")
}

func (wow *WorkOnWork) PaymentPhase() {
	var wg sync.WaitGroup
	fmt.Println("[💵] -------- 💵 LAUNCHING WAGE/UNEMPLOY. BEN. PAYMENT FOR ALL AGENTS 💵 -------- ")
	wow.printAgents()
	// GetAgents who dont work, pay them with unemploymentBenefit
	//for k, _ := range wow.workers {
	//	if wow.workers[k].EmployedBy == 0 {
	//		wow.workers[k].IncreaseSavings(wow.gov.GetUnemploymentBenefit())
	//
	//	}
	//}
	// Make companies pay their employees
	for k, _ := range wow.conFirms {
		wg.Add(1)
		go func(idx int) {
			defer wg.Done()
			wow.conFirms[idx].PayWage()
		}(k)
	}
	for k, _ := range wow.capFirms {
		wg.Add(1)
		go func(idx int) {
			defer wg.Done()
			wow.capFirms[idx].PayWage()
		}(k)
	}
	// make workers set up their savings
	for k, _ := range wow.workers {
		wg.Add(1)
		go func(idx int) {
			defer wg.Done()
			wow.workers[idx].SetSavingsOnWage(wow.gov.GetMinimumWage())
		}(k)
	}

	// The epoch ends when all agents have finished running
	wg.Wait()
	fmt.Println(" ----PAYMENT DONE-----")
	wow.printAgents()
	fmt.Println("[💵] -------- 💵 PAYMENT PHASE END 💵 -------- ")
}

func (wow *WorkOnWork) InnovationProcessSimulate() {
	fmt.Println("[💡] -------- 💡 LAUNCHING INNOVATION STEP 💡 -------- ")
	if wow.EmergenceOfRadicalInnovation() {
		log.Println("A new technological paradigm has been discovered")
		wow.highestGenerationEmerged += 1
	}
	for _, firm := range wow.capFirms {
		fmt.Println("Innovation is happening.")
		firm.Innovate(
			firm.AccessToIncrementalInnovation(
				wow.Param.SearchCapabilityForInnovation,
				firm.NormalizedShareOfInnovationWorkers()),
			firm.AccessToRadicalInnovation(
				wow.Param.AccessToNewGeneration,
				firm.NormalizedShareOfInnovationWorkers(),
			),
			wow.highestGenerationEmerged,
			wow.Param.ParamOfBetaDistribForIncrementalInnovation[0],
			wow.Param.ParamOfBetaDistribForIncrementalInnovation[1],
			wow.Param.BetaDistribSupportForIncrementalInnovation,
			wow.Param.EffectivenessOfOpportunitiesExploitation,
		)

		fmt.Println("Imitation is happening.")
		randomFirm := wow.capFirms[wow.RandomCapFirm()]
		firm.Imitate(
			firm.AccessToImitation(
				wow.Param.SearchCapabilityForImitation,
				firm.NormalizedShareOfImitationWorkers(),
			),
			randomFirm.Technology,
		)
		fmt.Println("Technologies are being produced.")
		firm.ProduceTechnologies(wow.Param.MarkUpInCapitalGoodSector)
	}
	fmt.Println("Innovation has happened.")
	fmt.Println("[💡] -------- 💡 INNOVATION PHASE END 💡 -------- ")

}

func (wow *WorkOnWork) EmergenceOfRadicalInnovation() bool {
	movingAverageOfAggregateIncome := utils.MovingAverageFloat64(wow.firmsAggregateIncome, 5)
	prob := 1 - math.Exp(-wow.Param.LikelihoodOfEmergence*movingAverageOfAggregateIncome)
	rand.Seed(time.Now().UnixNano())
	b := distuv.Bernoulli{P: prob}
	return utils.Ftob(b.Rand())
} // TODO: # calibrer le calcul des livraisons de technos; innovation; skills

func (wow *WorkOnWork) OrderingSimulate() {
	fmt.Println("Consumption-good firms order technologies to capital-good-sector.")
	for _, conFirm := range wow.conFirms {
		go func() {
			conFirm.ChooseTechnologyToOrder(wow.Param.DepreciationRateOfCapital)
			conFirm.OrderTechnologies()
		}()
	}
	c := make(chan int) // channel to know when the receiving step is finished
	for _, capFirm := range wow.capFirms {
		go capFirm.ReceiveOrders(c)
	}
	for i := 0; i < len(wow.capFirms); i += 1 {
		<-c
	}
	fmt.Println("Technologies have been ordered to capital-good-sector.")
}

func (wow *WorkOnWork) DeliveringSimulate() {
	fmt.Println("Delivery of technologies has begun.")
	for _, capFirm := range wow.capFirms {
		go capFirm.DeliverTechnologies()
	}
	c := make(chan int) // channel to know when the delivering step is finished
	for _, conFirm := range wow.conFirms {
		go conFirm.ReceiveDeliveries(c)
	}
	for i := 0; i < len(wow.conFirms); i += 1 {
		<-c
	}
	fmt.Println("Delivery of technologies has been done.")
}

func (wow *WorkOnWork) SetJobSearchChannels() {
	fmt.Println(" --- Setting Job Search Channels... --- ")
	wow.Lock()
	defer wow.Unlock()
	// For each worker, they receive a random set of firms
	firmCount := len(wow.conFirms) + len(wow.capFirms)
	totalMarketShare := float64(0)
	firmChannels := make([]chan agt.WorkerJobRequest, 0, firmCount)
	marketShares := make([]float64, 0, firmCount)
	for i, _ := range wow.conFirms {
		firmChannels = append(firmChannels, wow.conFirms[i].GetJobSearchChannel())
		marketShare := wow.conFirms[i].GetMarketShare()
		marketShares = append(marketShares, marketShare)
		totalMarketShare = totalMarketShare + marketShare
	}
	for i, _ := range wow.capFirms {
		firmChannels = append(firmChannels, wow.capFirms[i].GetJobSearchChannel())
		marketShare := wow.capFirms[i].GetMarketShare()
		marketShares = append(marketShares, marketShare)
		totalMarketShare = totalMarketShare + marketShare
	}
	for i, _ := range wow.workers {
		randomFirmSetIndex := wow.RandomFirmSet(marketShares, totalMarketShare, wow.jbRandJobs)
		// If already employed, filter out the company in which the worker is:
		var randomFirmSetIndexFiltered []int
		for _, firmIdx := range randomFirmSetIndex {
			if firmIdx+1 != wow.workers[i].EmployedBy {
				randomFirmSetIndexFiltered = append(randomFirmSetIndexFiltered, firmIdx)
			}
		}

		//fmt.Println("Worker number", i+1, "has selected firms of id: ", randomFirmSetIndexFiltered)
		randomFirmSet := make([]chan agt.WorkerJobRequest, 0, len(randomFirmSetIndexFiltered))
		for _, idx := range randomFirmSetIndexFiltered {
			randomFirmSet = append(randomFirmSet, firmChannels[idx])
		}
		//wow.workers[i].EmptyPreviousChannels(wow.jbRandJobs)
		wow.workers[i].SetFirmChannels(randomFirmSet)
	}
	fmt.Println(" --- Job Search Channels Set --- ")
}

// RandomFirmSet : gives a set of int ID of firms choosed randomly but proportionnally according to their market share
func (wow *WorkOnWork) RandomFirmSet(shares []float64, totalMarket float64, numberJobs int) []int {
	randomFirmSetIndex := make([]int, 0, numberJobs)
	for i := 0; i < numberJobs; i++ {
		newJobIdx := utils.Randombin(shares, totalMarket)
		for utils.Contains(randomFirmSetIndex, newJobIdx) {
			newJobIdx = utils.Randombin(shares, totalMarket)
		}
		randomFirmSetIndex = append(randomFirmSetIndex, newJobIdx)
	}
	return randomFirmSetIndex
}

// RandomConFirm : gives an int ID of one consumption-good firm chosen randomly (not proportionally to the market share)
func (wow *WorkOnWork) RandomConFirm() int {
	rand.Seed(time.Now().UnixNano())
	return rand.Intn(len(wow.conFirms))
}

// RandomCapFirm : gives an int ID of one capital-good firm chosen randomly (not proportionally to the market share)
func (wow *WorkOnWork) RandomCapFirm() int {
	rand.Seed(time.Now().UnixNano())
	return rand.Intn(len(wow.capFirms) - 1)
}

func (wow *WorkOnWork) SignalMachinesSimulate() {
	// Send technologies catalogs
	allCatalogs := make([]agt.Catalog, 0)
	for _, capFirm := range wow.capFirms {
		capFirm.OrderChan = make(chan agt.Order)
		catalog := agt.Catalog{
			Technology:   capFirm.Technology,
			FirmProdChan: capFirm.OrderChan,
		}
		allCatalogs = append(allCatalogs, catalog)
	}
	for _, conFirm := range wow.conFirms {
		conFirm.ReceiveCatalogs(allCatalogs)
	}
}

func (wow *WorkOnWork) UpdateAggregateProductivity() {
	ap := 1.0
	for _, firm := range wow.conFirms {
		ap += firm.EffectiveProductivity[len(firm.EffectiveProductivity)-1]
	}
	wow.aggregateProductivity = append(wow.aggregateProductivity, ap)
	for _, firm := range wow.capFirms {
		ap += firm.EffectiveProductivity[len(firm.EffectiveProductivity)-1]
	}
	wow.aggregateProductivity = append(wow.aggregateProductivity, ap)
}

func (wow *WorkOnWork) UpdateAggregateWages() {
	aw := 0.0
	for _, worker := range wow.workers {
		aw += worker.Wage
	}
	wow.aggregateWages = append(wow.aggregateWages, aw)
}

func (wow *WorkOnWork) UpdateAverageWage() {
	wow.averageWage = append(wow.averageWage, wow.aggregateWages[len(wow.aggregateWages)-1]/float64(len(wow.workers)))
}

func (wow *WorkOnWork) UpdateConFirmsAggregateIncome() {
	sortedConFirmsIncome := wow.sortConFirmsByPeriodicIncome()
	sum := 0.0
	for _, income := range sortedConFirmsIncome {
		sum += income
	}
	wow.conFirmsAggregateIncome = append(wow.conFirmsAggregateIncome, sum)
}

func (wow *WorkOnWork) UpdateCapFirmsAggregateIncome() {
	sortedCapFirmsIncome := wow.sortCapFirmsByPeriodicIncome()
	sum := 0.0
	for _, income := range sortedCapFirmsIncome {
		sum += income
	}
	wow.capFirmsAggregateIncome = append(wow.capFirmsAggregateIncome, sum)
}

func (wow *WorkOnWork) UpdateFirmsAggregateIncome() {
	ai := wow.Param.InitialFirmsAggregateIncome
	if len(wow.capFirmsAggregateIncome) != 0 && len(wow.conFirmsAggregateIncome) != 0 {
		capFirmAggInc := wow.capFirmsAggregateIncome[len(wow.capFirmsAggregateIncome)-1]
		conFirmAggInc := wow.conFirmsAggregateIncome[len(wow.conFirmsAggregateIncome)-1]
		ai = capFirmAggInc + conFirmAggInc
	}

	wow.firmsAggregateIncome = append(wow.firmsAggregateIncome, ai)
}

func (wow *WorkOnWork) UpdateTechnologiesSold() {
	techSold := 0
	for _, capFirm := range wow.capFirms {
		techSold += len(capFirm.Orders)
	}
	wow.technologiesSold = techSold
}

func (wow *WorkOnWork) UpdateAggregateVariables() {
	wow.UpdateAggregateWages()
	wow.UpdateAverageWage()
	wow.UpdateConFirmsAggregateIncome()
	wow.UpdateCapFirmsAggregateIncome()
	wow.UpdateFirmsAggregateIncome()
	wow.UpdateTechnologiesSold()
}

func (wow *WorkOnWork) InitializeFirmVariables() {
	wow.Lock()
	defer wow.Unlock()
	// General Firm variables
	//- EffectiveProductivity
	//- Demand

	for _, conFirm := range wow.conFirms {
		conFirm.Demand = append(conFirm.Demand, float64(len(wow.workers)/len(wow.conFirms))) // initializing at (simplifying) workers wage divided by the number of firm
		conFirm.UpdateVariables()
	}
	for _, capFirm := range wow.capFirms {
		capFirm.UpdateVariables(wow.Param.ShareOfRDExpenditureInImitation, wow.Param.RDInvestmentPropensityOverSales)
	}
}

func (wow *WorkOnWork) ConsumptionSimulate() {
	consumption := 0.0
	fmt.Println("[🍔] ------- 🍔 WORKERS' CONSUMPTION PHASE 🍔 ---------")

	for _, conFirm := range wow.conFirms {
		conFirm.CurrentDemand = 0
	}

	for _, worker := range wow.workers {
		consumption = worker.WorkerConsumption()
		fmt.Println(consumption, worker.ID)
		wow.conFirms[wow.RandomConFirm()].IncreaseDemand(consumption)
	}

	for _, conFirm := range wow.conFirms {
		if conFirm.CurrentDemand < 0 {
			//fmt.Println(conFirm.ID, conFirm.CurrentDemand)
			panic("current demand <= 0!")
		}
		conFirm.Demand = append(conFirm.Demand, conFirm.CurrentDemand)
		log.Println("Consumption good firm", conFirm.Firm.ID, "'s current demand (and thus income) is", conFirm.CurrentDemand)
	}
}

func (wow *WorkOnWork) ReportWorldState() agt.WorldState {
	// sorted array of the wages for certain values
	sortedWorkerWages := wow.sortWorkersByWages()
	avgWage := utils.MeanFloat(sortedWorkerWages)
	medWage := sortedWorkerWages[len(sortedWorkerWages)/2-1]
	maxWage := sortedWorkerWages[len(sortedWorkerWages)-1]
	wwDeciles := utils.GetNQuantiles(sortedWorkerWages, 10.0)

	u := len(utils.Filter(wow.workers, func(worker *agt.Worker) bool { return !worker.IsEmployed() }))

	// counting the number of technologies created
	techCount := 0
	var generationPerFirm []int
	avgGen := 0
	for _, capFirm := range wow.capFirms {
		log.Println("firm has", capFirm.NumberOfTechnologies)
		techCount += capFirm.NumberOfTechnologies
		generationPerFirm = append(generationPerFirm, capFirm.CurrentTechnologicalGeneration)
		avgGen += capFirm.CurrentTechnologicalGeneration
	}
	avgGen = avgGen / len(wow.capFirms)
	return agt.WorldState{
		Epoch: wow.currentEpoch,

		Active: wow.Active,

		ConsFirmCount: len(wow.conFirms),
		CapFirmCount:  len(wow.capFirms),

		WorkerCount:     len(wow.workers),
		UnemployedCount: int(u),

		AggrLabourProd: wow.aggregateProductivity[len(wow.aggregateProductivity)-1],
		//AggrLabourProdByFirm []float64 `json:"aggr-labour-prod-by-firm"`

		Unemployment: float64(u) / (float64(u) + float64(len(wow.workers))),

		MinimumWage:       wow.gov.GetUnemploymentBenefit(),
		AverageWage:       avgWage,
		MedianWage:        medWage,
		MaximumWage:       maxWage, //doesn't work
		WorkerWageDeciles: wwDeciles,

		CapFirmsIncome: wow.capFirmsAggregateIncome[len(wow.capFirmsAggregateIncome)-1],
		ConFirmsIncome: wow.conFirmsAggregateIncome[len(wow.conFirmsAggregateIncome)-1],
		FirmsIncome:    wow.firmsAggregateIncome[len(wow.firmsAggregateIncome)-1],

		//AverageImitationRate float64 `json:"average-imitation-rate"`
		//AverageRadicalInnovationRate float64 `json:"average-radical-innovation-rate"`
		CreatedTechnologiesCount: 		techCount,
		TechnologiesSold:							wow.technologiesSold,
		MinimumTechGeneration:    		utils.Optimum(generationPerFirm, func(techGen1, techGen2 int) bool { return techGen1 < techGen2 }),
		AverageTechGeneration:    		avgGen,
		MaximumTechGeneration:    		utils.Optimum(generationPerFirm, func(techGen1, techGen2 int) bool { return techGen1 > techGen2 }),
		HighestTechGenerationEmerged: wow.highestGenerationEmerged,

		AggregatedConsumption: 0,
	}
}

func (wow *WorkOnWork) GetAverageWage() float64 {
	sum := float64(0)
	for _, worker := range wow.workers {
		sum += worker.Wage
	}
	return sum / float64(len(wow.workers))
}

func (wow *WorkOnWork) sortWorkersByWages() []float64 {
	var wages []float64

	// filling wages array with values
	for _, worker := range wow.workers {
		if worker.IsEmployed() {
			wages = append(wages, worker.Wage)
		} else {
			wages = append(wages, wow.gov.GetUnemploymentBenefit())
		}
	}

	// sorting wages array
	return utils.Sort(wages, func(wage1, wage2 float64) bool { return wage1 > wage2 })
}

func (wow *WorkOnWork) sortConFirmsByPeriodicIncome() []float64 {
	var incomes []float64

	// filling wages array with values
	for _, conFirm := range wow.conFirms {
		incomes = append(incomes, conFirm.CurrentDemand) // Demand is monetary is our case so equals income
	}

	// sorting incomes array
	return utils.Sort(incomes, func(income1, income2 float64) bool { return income1 > income2 })
}

func (wow *WorkOnWork) sortCapFirmsByPeriodicIncome() []float64 {
	var incomes []float64

	// filling wages array with values
	for _, capFirm := range wow.capFirms {
		incomes = append(incomes, capFirm.CurrentDemand) // Demand is monetary is our case so equals income
	}

	// sorting wages array
	return utils.Sort(incomes, func(income1, income2 float64) bool { return income1 > income2 })
}
