package wow

type Param struct {
	//********- INNOVATION & IMITATION -********//
	LikelihoodOfEmergence                      float64
	AccessToNewGeneration                      float64
	SearchCapabilityForImitation               float64
	SearchCapabilityForInnovation              float64
	ParamOfBetaDistribForIncrementalInnovation []float64
	BetaDistribSupportForIncrementalInnovation []float64
	EffectivenessOfOpportunitiesExploitation   float64
	ShareOfRDExpenditureInImitation            float64
	PaybackPeriodForMachineReplacement         int
	RDInvestmentPropensityOverSales            float64

	//*********- LABOUR MARKET -**********//

	SkillAccumulationRateOnTenure        float64
	SkillDeteriorationRateOnUnemployment float64
	AggregateProductivityPassThrough     float64
	MinimumDesiredWageIncreaseRate       float64

	//*********- TECHNOLOGIES TRANSFERS -********//

	DepreciationRateOfCapital float64
	MarkUpInCapitalGoodSector float64

	//*********- INITIAL PARAMETERS FOR TECHNOLOGIES -********//

	InitialLabourProductivityOfTechnology            float64
	InitialLabourProductivityNeededToBuildTechnology float64

	//*********- GOVERNMENT -*********//

	TaxRate                                     float64
	InitialMinimumWage                          float64
	InitialUnemploymentBenefit                  float64
	FractionOfAverageWageForUnemploymentBenefit float64
	RetiringAge                                 int
	InitialEmploymentRatio                      float64

	//*********- INITIAL VARIABLES OF ABM -*********//

	NumberOfEpochs               int
	NumberOfConsumptionGoodFirms int
	NumberOfCapitalGoodFirms     int
	NumberOfWorkers              int
	InitialAge                   int
	InitialSkills                float64
	InitialOfferedWage           float64
	InitialOfferdWagePremium     float64
	InitialPastSales             []float64
	InitialMarketShare           float64
	InitialExpectedProduction    float64
	InitialFirmsAggregateIncome  float64
}

func getParameters() Param {
	ParamOfBetaDistribForIncrementalInnovation := []float64{3, 3}
	BetaDistribSupportForIncrementalInnovation := []float64{-0.150, 0.150}
	return Param{

		//********- INNOVATION & IMITATION -********//

		LikelihoodOfEmergence:                      0.0005, //changed to faster results, init: 0.03
		AccessToNewGeneration:                      0.020,
		SearchCapabilityForImitation:               0.100,
		SearchCapabilityForInnovation:              0.100,
		ParamOfBetaDistribForIncrementalInnovation: ParamOfBetaDistribForIncrementalInnovation,
		BetaDistribSupportForIncrementalInnovation: BetaDistribSupportForIncrementalInnovation,
		EffectivenessOfOpportunitiesExploitation:   0.200,
		ShareOfRDExpenditureInImitation:            0.500,
		PaybackPeriodForMachineReplacement:         9,
		RDInvestmentPropensityOverSales:            0.040,

		//*********- LABOUR MARKET -**********//

		SkillAccumulationRateOnTenure:        0.010,
		SkillDeteriorationRateOnUnemployment: 0.010,
		AggregateProductivityPassThrough:     1.000,
		MinimumDesiredWageIncreaseRate:       0.020,
		RetiringAge:                          62,
		InitialEmploymentRatio:               0.8,

		//*********- TECHNOLOGIES TRANSFERS -********//

		DepreciationRateOfCapital: 0.150,
		MarkUpInCapitalGoodSector: 0.100,

		//*********- INITIAL PARAMETERS FOR TECHNOLOGIES -********//

		InitialLabourProductivityOfTechnology:            1.000,
		InitialLabourProductivityNeededToBuildTechnology: 1.000,

		//*********- GOVERNMENT -*********//

		TaxRate:                    0.100,
		InitialMinimumWage:         0.500,
		InitialUnemploymentBenefit: 0.500,
		FractionOfAverageWageForUnemploymentBenefit: 0.400,

		//*********- INITIAL VARIABLES OF ABM -*********//

		NumberOfEpochs:               100, // 100 at least for a good simulation
		NumberOfConsumptionGoodFirms: 25,
		NumberOfCapitalGoodFirms:     10,
		NumberOfWorkers:              25000,
		InitialAge:                   1, // or a variable set as rand.Intn(60-19+1)+19
		InitialSkills:                1.0,
		InitialOfferedWage:           1,
		InitialOfferdWagePremium:     0.1,
		InitialPastSales:             []float64{1000},
		InitialMarketShare:           1000, // to be randomized
		InitialExpectedProduction:    1000,
		InitialFirmsAggregateIncome:  50000, // the number of workers of consumption = 1

	}
}
