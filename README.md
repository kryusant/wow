# WorkonWork.ai project 💼💵


## I) How the project works

Our project "WorkOnWork" is part of the ambition to do an extensive research on the problem of employment and automation (associated RP), and to make a critical analysis of the relationship between the labor market and technological innovation. To this end, the objective of this project is to establish a simulation of the labor market that orchestrates the phenomenon of technological change and its impact on capital productivity and the wage bill, based on a scientific article of an evolutionary ABM.

To do so, our simulation aims at replicating key economic phenomena of the labor market and production, by injecting the concept of technological tool generation. It therefore sequentially coordinates 7 main steps that are repeated in cycles ("periods", "epochs") which are listed below.

#### Agents

The economic agents in our simulation are:
* the **technology firms**; perform R&D and produce the technologies to improve the productivity of the consumer firms,
* the **consumer firms**,
* the **workers**,
* the **government**; for the moment, it is in charge of adjusting and providing unemployment benefits.

#### Model construction

Of course the model established is a simplified model of the article itself, and does not take into account a number of important macroeconomic events such as inter-industry competition (industry agents) and investment by the financial sector (bank agents).

Great importance has been attached to seeing the construction of the model over time. For example, the government and technologies were considered as "agents" in our discussions, whereas they are in fact large structures helped by the agents themselves, including the coordinator, to allow them to be assigned properties or to communicate.

#### Epochs

The 7 steps of a simulation epoch (or cycle):
1. The technology firms deliver tools to the consumption firms (reserved in their order book) of the previous period.
2. Technology firms conduct R&D and create new paradigm technologies, or incremental improvements of previous generations, or technologies imitated from their neighbors. They report their production to consumer firms.
3. Consumer firms choose which technologies to order for the next epoch.
4. According to their production projections, firms open applications for their jobs. Workers (employed or unemployed), apply to a subset of firms and accept the best offer that is made to them.
5. Firms pay wages and bonuses. The government pays unemployment benefits.
6. Workers consume the products sold by the consumer firms with part of their wages.
7. Firms determine their production projections for the next epoch.

##### Example of steps 4 and 5 of the labour market phase

The job search process is done in stages, but all agents engage in this phase in parallel. The first phase consists in sending applications: the `Workers` apply to a set of randomly determined companies (`Firm`). The probability of applying to a firm is defined in the model as proportional to the firm's market share.
Secondly, the firms will receive applications up to a certain deadline, if they require employees, and then select the best candidates. The selection criterion is the salary required by the "Worker" and the number of "Workers" required by the company.
The `Workers`, waiting since the sending of their application, then receive answers progressively from the companies. After a certain period of time, the `Workers` decide which of the proposals received and their current job (if they are employed) is the best according to their "utility", which is the salary offered by the companies that have accepted the application. If a proposal is better than a Worker's current job, he resigns from the company.
Finally, the `Worker` sends his answer (positive or negative) and if the answer is positive, he is added to the `Workforce` of the company and starts receiving a salary from the current iteration.
Then, the salary payment phase takes place, each `Worker` will have his wage modified and he will use it for the consumption phase.

## II) Installation of the project, launching and architecture

#### Linux instructions

For React (user interface and website of the project):
- Donwload and install [Node.Js](https://nodejs.org/en/);
- Donwload and install [npm](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm);
- Launch in the `react-app` folder of the project to install React and all dependencies:
```
npm i
```

For Go code (the agent-based model coded):
- Donwload and install the latest version of [Go](https://go.dev/doc/install).

#### Launching the app

Without order:
- Launch React interface in the `react-app` folder:
```
npm start
```
- Launch Go application (simuation and server) in the `abm` folder:
```
go run ./cmd/launch_app.go
```
On first launch, Go will automatically download the dependencies.

#### Architecture

##### Technical architecture

The project is composed of a **React** web application, deploying all the graphical elements and allowing to launch the simulation. The backend program is a **Go** server running the code of our simulation.

##### Fonctionnalités

The server delivers a RESTful API through which the client can access the `Report` resource through the `epoch_done` *route which is a set of variables aggregating some simulated data (e.g. unemployment), of interest to see the consistency of the model. The React application retrieves these data and processes them to add them to lists of elements composing graphs from the **Plotly** library, and updates these graphs in real time with the retrieved data every second, updated only if the time is different from the current one. There are also other *routes*, present to receive React requests from the **Start** and **Stop** buttons, which respectively allow to launch the simulation (by doing a **Reset** of the multi-agent system if it has already been launched and stopped by the *Stop* button) and to deactivate the model (which will stop at the end of the current epoch). The architecture is thus ready to incorporate in the future an interactive change of the numerous parameters of the model.

##### Code architecture

The system in the Go model is centered around the `WorkOnWork` struct, which acts as a coordinator of the model and thus is composed of all our agents, a large parameter structure, as well as the various calculations responsible for the sequence of events (steps) desired in our simulation. During the launch of the simulation, a new `WorkOnWork` object is created, and the agents and parameters are provided before launching the `Simulate` function, whose role is to initialize the simulation, and then to run it for a given number of epochs.
A cycle (`epoch` in the code) of simulation is supposed to follow the timeline described above in part 1.
At the end of each epoch, a serializable `WorldState` struct is generated and reported in the `Report` attribute of the `WorkOnWork` object, containing all the analytically relevant variables. This can then be sent in response to the query periodically executed by our React client (function `fetch`).

An epoch is divided into functions along the stages of the timeline because the agents must be coordinated to follow a structured path. Each function will one by one launch or use the agents involved in the corresponding macro phase.

##### Interagent communication

For the job search phase (as well as for the order and delivery of technologies between firms) the agents communicate via channels. For each step, a different type of channel is used, because for example for the job search, the nature of the messages is different:
- for the first stage a `Worker` communicates to a firm his desired salary but;
- for the second step, the company will communicate if it retains the candidate or not and what is the salary it offers;
- for the third step, the candidate will notify his decision via a boolean to the companies having accepted him and to his current company if he decides to leave it.

Nevertheless, for these steps, the communication principle remains the same: an agent sends a message to another agent via the channel provided to him and in his message, he sends the response channel on which he will be listening until a certain time. We can see this delay as the "patience" of the agent to be calibrated obviously according to the calculation time taken by the computer which produces the simulation.

## III) Positive and negative points of the project, variation analysis of metaparameters

##### Assets and interest

The positive points of the project are that we have succeeded in establishing an interaction between the agents, who each maximize their profit (a utility that is specific to their situation) while counting on a "patience" that is translated into the model by *time-outs*. The added value could be beneficial for this domain if open-source access to the development of the application would be allowed. The more detailed knowledge acquired on the economics of innovation (evolutionary models), and the economics of labor, is also a great asset.

##### Calibration step

A large structure in `parameters.go` allows us to see all the parameters of the model, as well as the initialization data of some agents, for practical purposes for the calibration of the results, even if the formulas of the functions to be modified are to be found in the debugging. 
Nevertheless, the calibration of the model is one of the biggest challenges we had to face during this project. Indeed, it was necessary to calibrate the times for the different sequences of interaction between the agents and the economic parameters describing the simulated world, because the formulas for estimating wages, production, consumption and hiring depend on them. The result is currently a partially finished model since, although almost all the desired functions have been coded, they have not yet been well calibrated. Validation, for example Monte Carlo validation by repetition, has not been implemented and would have been necessary, once the model has been calibrated, to see if this calibration will be successful.

##### Viability of the project for the proof of concept phase (course "IA04")

The project was also too ambitious to be done in 2 months (coded in a little more than a month, first push on December 2nd, the remaining time being spent on modeling with 4 not counting the RP), which is a pity because we would have liked to have more results and to build a graphical interface representing the interactions between groups of agents (the researchers having arrived at the model of the paper working on evolving, running and calibrating evolutionary multi-agent systems for 40 years). The model was difficult to implement, because of its size (number and complexity of functions) and the many phases it was intended to simulate.

##### First results

As for the analysis of the parameters and metaparameters, we would need to test more configurations. Our first results where the wages of our workers were exponential after about 30 epochs show us that calibration is of paramount importance as we already knew, because in the real world, indeed some people have exponential wages, but it does not result in the same thing for everyone. The results of unemployment, for example, are understood with respect to the functions but notably because the calculations on the variables that are related to unemployment were sometimes simplified (example of the *RequiredWorkforce*).

## IV) Appendices

#### 0) Link to gitlab repository

[Link to the git](https://gitlab.utc.fr/operezal/ia04-projet-wow)

#### 1) Schema for understanding the interaction of agents
![](https://md.picasoft.net/uploads/upload_b2557b4849f2730e944b3fc37924b2b9.png)

#### 2) Excerpt from the working document on functions, agents, and model parameters to design the UML for agents
[Link to the working document](https://wwwetu.utc.fr/~kryusant/Model_info.pdf)

#### 3) URL of the article on which the model is based
[G. Dosi , M.C. Pereira , A. Roventini , M.E. Virgillito, "Technological paradigms, labour creation and destruction in a multi-sector agent-based model", Research Policy, 51 (2022)](https://www.sciencedirect.com/science/article/abs/pii/S0048733322000889?via%3Dihub)

#### 4) Sequence diagrams for the labor market and the technology market phases
![](https://md.picasoft.net/uploads/upload_e79b5c5d1bd408ad785fa2a8a87c5170.png)
